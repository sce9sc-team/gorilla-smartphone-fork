gorilla.socialSharingData = 
{
	fb:
	{
		products_fb:function(){
			return "page2"
		},	
		
		fun_fb:function(){
			return "page26"
		},
		
		news_fb:function(){
			return "page18"
		},
		
		productsfull_fb:function(){
			return "page3"
		},
		
		faq_fb:function(){
			return "page41"
		},
		
		specificnews_fb:function(){
			return "page19"
		},
				
		
		innovating_fb:function(){
			//Innovating
			if (selectedTabOpen == '')// whole page
			{				
				return "page20"
			} 
			else if (selectedTabOpen == 'accBar0')// OVERVIEW
			{				
				return "page21"
			} 
			else if (selectedTabOpen == 'accBar1')// CHARACTERISTICS
			{				
				return "page22"
			} 
			else if (selectedTabOpen == 'accBar2')// CUSTOMIZATION
			{				
				return "page23"
			} 
			else if (selectedTabOpen == 'accBar3')// APPLICATIONS
			{				
				return "page24"
			} 
			else if (selectedTabOpen == 'accBar4')// LITERATURE
			{				
				return "page25"
			}
		},
		
		videos_fb:function(){
			//Videos
			if (selectedVideoTabOpen === -1)// no video selected, just share the page
			{								
				return "page32"
				//var args = [ "trackEvent", "GorillaVideos", "FacebookShares", "Videos page", "Videos page" ];
				//fiveml._invokeTracking(args);
			} 
			else if (selectedVideoTabOpen === 0)
			{
				if (selectedVideoToShare === 0)// channel surfing
				{
					page = "page33";
					
					//var args = [ "trackEvent", "GorillaVideos", "FacebookShares", "Channel Surfing video", "Videos page" ];
					//fiveml._invokeTracking(args);
				} 
				else if (selectedVideoToShare === 1)// cooking up tomorrow's kitchen
				{
					page = "page34";
					
					//var args = [ "trackEvent", "GorillaVideos", "FacebookShares", "Cooking Up Tomorrow's Kitchen video", "Videos page" ];
					//fiveml._invokeTracking(args);
				} else if (selectedVideoToShare === 2)// king of the office
				{
					page = "page35";
					
					//var args = [ "trackEvent", "GorillaVideos", "FacebookShares", "King of the Office video", "Videos page" ];
					//fiveml._invokeTracking(args);
				}
			}
			else if (selectedVideoTabOpen === 1)
			{
				if (selectedVideoToShare === 0)// How Corning Tests Gorilla Glass
	            {
	            	page = "page51";
	            	// analytics
	            	var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
	            			"How Corning Tests Gorilla Glass", "Videos page" ];
	            	fiveml._invokeTracking(args);
	            }   
			}
			else if (selectedVideoTabOpen === 2)
			{
				if (selectedVideoToShare === 0)// gorilla tough
				{
					page = "page38";
					// analytics
					var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
							"Gorilla Tough video", "Videos page" ];
					fiveml._invokeTracking(args);
				}
	            else if (selectedVideoToShare === 1)// Microsoft Puts Corning Gorilla Glass 2 to the Test
	            {
	            	page = "page50";
	            	// analytics
	            	var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
	            			"Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos page" ];
	            	fiveml._invokeTracking(args);
	            }        
			}
			else if (selectedVideoTabOpen === 3)
			{
				if (selectedVideoToShare === 0)// a day made of
				{
					page = "page39";
					// analytics
					var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
							"A Day Made of Glass video", "Videos page" ];
					fiveml._invokeTracking(args);
				}
	            else if (selectedVideoToShare === 1)// Corning Gorilla Glass for Seamless Elegant Design
	            {
	                page = "page49";
	            	// analytics
	            	var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
	            			"Corning Gorilla Glass for Seamless Elegant Design", "Videos page" ];
	            	fiveml._invokeTracking(args);
	            } 
	            else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
	            {
	                page = "page52";
	                // analytics
	            	var args = [ "trackEvent", "GorillaVideos", "FacebookShares",
	            			"Changing the Way We Think about Glass", "Videos page" ];
	            	fiveml._invokeTracking(args);
	            }
			}
		},
		
				
		wallpaper_fb:function(){
			//Wallpapers
			if (currentId == 'coverflow0pic1')// fatsa
			{				
				return "page31"
				
				//var args = [ "trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla face", "Wallpaper - Gorilla face" ];
				//fiveml._invokeTracking(args);
			} 
			else if (currentId == 'coverflow0pic2')// filaei kato
			{				
				return "page29"
				
				//var args = [ "trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Kiss", "Wallpaper - Gorilla Kiss" ];
				//fiveml._invokeTracking(args);
			} 
			else if (currentId == 'coverflow0pic3')// kornizes
			{				
				return "page30"
				
				//var args = [ "trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Frame", "Wallpaper - Gorilla Frame" ];
				//fiveml._invokeTracking(args);
			} 
			else if (currentId == 'coverflow0pic4')// bananes
			{				
				return "page28"
				
				//var args = [ "trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Banana", "Wallpaper - Gorilla Banana" ];
				//fiveml._invokeTracking(args);
			} 
			else if (currentId == 'coverflow0pic5')// Fossey
	        {	        	
	        	return "page48"
	        	
	        	//var args = [ "trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Fossey", "Wallpaper - Gorilla Fossey" ];
	        	//fiveml._invokeTracking(args);
	        }
		},
		
				
		//Products
		acer_fb:function(){
			return "page4"
		},
		
		dell_fb:function(){
			return "page6"
		},
		
		htc_fb:function(){
			return "page7"
		},
		
		kyo_fb:function(){
			return "page8"
		},
		
		asus_fb:function(){
			return "page5"
		},
		
		lg_fb:function(){
			return "page10"
		},
		
		moto_fb:function(){
			return "page11"
		},
		
		moti_fb:function(){
			return "page12"
		},
		
		nec_fb:function(){
			return "page13"
		},
		
		nokia_fb:function(){
			return "page14"
		},
		
		sam_fb:function(){
			return "page15"
		},
		
		sk_fb:function(){
			return "page16"
		},
		
		sony_fb:function(){
			return "page17"
		},
		
		lenovo_fb:function(){
			return "page9"
		},
		
		hyundai_fb:function(){
			return "page40"
		},
		
		samsungSUR40_fb:function(){
			return "page46"
		},
		
		hp_fb:function(){
			return "page45"
		},
		
		praise_fb:function(){
			return "page44"
		},
		
		allpraise_fb:function(){
			return "page44"
		},
		
		sonim_fb:function(){
			return "sonim_fb"
		},
		
		lumigon_fb:function(){
			return "page53"
		},
	
	}
}


	
		