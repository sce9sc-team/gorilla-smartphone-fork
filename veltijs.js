var veltijs = {};


veltijs.createloadingBar = function()
{
    var loading =  document.getElementById("progressbar");
    var loadingbar =  document.getElementById("loadingbar");
    loading.style.background = "transparent"
    var indicator =  document.getElementById("indicator");
    indicator.style.height="12px";
    indicator.style.background = "#c8c8c8"
    loadingbar.style.display="block";
}
veltijs.setloadingBar = function(moduleNum,modules)
{
    var progressbar =document.getElementById("progressbar").style.width.slice(0,-2);
    var indicator = document.getElementById("indicator");
    var loadinginfo =  document.getElementById("loadinginfo");
    //loadinginfo.innerHTML = "Loading assets :"+modules[moduleNum];
    indicator.style.width = (progressbar/modules.length)*moduleNum +"px";
}
veltijs.hideLoadingBar = function()
{
     var loadingbar =  document.getElementById("loadingbar");
     loadingbar.style.display="none";
}

veltijs.analytics = function(){
	var gastaging = veltijs.analyticsAccount.google.staging;
	var visualizeStaging = veltijs.analyticsAccount.visualize.staging;
	
	var analyticsScript = document.createElement('script');
	analyticsScript.setAttribute('type','text/javascript'); 
	
	var scriptCont = " var _gaq = _gaq || [];  _gaq.push(['_setAccount', '"+gastaging+"']); "+
		"(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();";
	
	analyticsScript.innerHTML = scriptCont;
	
	var visualizeAnalyticsScript  = document.createElement('script'); 
	visualizeAnalyticsScript.setAttribute('type','text/javascript'); 
	
	var visualizescriptCont = 'var _paq = _paq || [];'+
		'(function(){ var u=(("https:" == document.location.protocol) ? "https://'+visualizeStaging.PIWIK_URL+'/" : "http://'+visualizeStaging.PIWIK_URL+'/");'+
		'_paq.push(["setSiteId", '+visualizeStaging.IDSITE+']);'+
		'_paq.push(["setTrackerUrl", u+"piwik.php"]);'+
		'_paq.push(["enableLinkTracking"]);'+
		'_paq.push(["setCustomUrl","'+document.location.origin+document.location.pathname+'"]);'+
		'var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript"; g.defer=true; g.async=true; g.src=u+"piwik.js";'+
		's.parentNode.insertBefore(g,s); })();';
	
	visualizeAnalyticsScript.innerHTML = visualizescriptCont;
	
	document.getElementsByTagName('head')[0].appendChild(analyticsScript);
	document.getElementsByTagName('head')[0].appendChild(visualizeAnalyticsScript);
};

veltijs.analyticsTrackPage = function(enter_page)
{
	//piwikTracker.trackPageView('Menu/Freedom')
	var visualizeStaging = veltijs.analyticsAccount.visualize.staging;	
	var pageName = veltijs.getAnalytics.pageAnalytics[enter_page.id];
	
	_paq.push(["setSiteId", visualizeStaging.IDSITE]);
	_gaq.push(['_trackPageview',pageName]);
	_paq.push(["trackPageView",pageName]);
}

veltijs.analyticsEventTracking = function(objA)
{
	/*
	 _trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
		category (required)
		The name you supply for the group of objects you want to track.
		
		action (required)
		A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.
		
		label (optional)
		An optional string to provide additional dimensions to the event data.
		
		value (optional)
		An integer that you can use to provide numerical data about the user event.
		
		non-interaction (optional)
		A boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation. 
	 
	 */
	var visualizeStaging = veltijs.analyticsAccount.visualize.staging;
	_gaq.push(['_trackEvent', objA.cat, objA.act, objA.lbl]);
	_paq.push(["setSiteId",  visualizeStaging.EVENTSITEID]);
	_paq.push(["trackPageView",objA.cat+"/"+objA.act+"/"+objA.lbl]);
	
};

veltijs.analyticsEventListener = function(e)
{
	
	var objA = veltijs.getAnalytics[e.target.getAttribute("data-analytics")];
	console.log(objA)
	veltijs.analyticsEventTracking(objA);	
};

veltijs.pushCustomEventAnalytics = function(obj)
{
	var objA = veltijs.getAnalytics[obj];
	veltijs.analyticsEventTracking(objA);	
}

veltijs.setAnalyticsEvents = function(el)
{
	var el_analytics_div = el.querySelectorAll('div[data-analytics]');
	var el_analytics_img = el.querySelectorAll('img[data-analytics]');
	for(var i=0;i<el_analytics_div.length;i++)
	{
		el_analytics_div[i].addEventListener('click',veltijs.analyticsEventListener);
	};
	for(var i=0;i<el_analytics_img.length;i++)
	{
		el_analytics_img[i].addEventListener('click',veltijs.analyticsEventListener);
	};
};



veltijs.addSwipe = function(id, funcA, funcB,dir)
{
	
	//alert(document.body.clientWidth)
	var k = document.getElementById(id);
	//alert(window.navigator.userAgent);
	veltijs.movebias = 50;
	if(window.navigator.userAgent.toLowerCase().search("android")>-1)
	{	
		veltijs.movebias = 25;
		if (window.navigator.userAgent.toLowerCase().search("gt-p1000")>-1)
		{
			veltijs.movebias = 70;
		}
		if (window.navigator.userAgent.toLowerCase().search("a100")>-1)
		{
			veltijs.movebias = 7;
		}
		if (window.navigator.userAgent.toLowerCase().search("gt-i9300")>-1)
		{
			veltijs.movebias = 7;
		}
	}
	if (window.navigator.userAgent.toLowerCase().search("blackberry")>-1)
	{
		veltijs.movebias = 7;
	}
	
	function doSwipe(fn,e)
	{
		
		if (k.anim) 
		{
			 k.anim = false; fn(e.target,k); 
		}
	}
	
	k.anim    = false;
	k.capture = false;
	
	if('ontouchstart' in document)
	{
		k.ontouchstart= function(e)
		{				
			veltijs.scrollmove = false;
			k.swipeStart =	k.swipeMove  =	(dir === undefined ? e.targetTouches[0].pageX : e.targetTouches[0].pageY);
			k.pageYOffset1 = k.pageYOffset2 = window.pageYOffset;
			
			k.anim = true;
			k.noAnim= true;
			if (!e) var event = window.event;
			e.cancelBubble = true;
			if(dir!==undefined)e.preventDefault();
			if (e.stopPropagation) e.stopPropagation();			
		};
			
		k.ontouchmove = function(e)
		{
			
			if (!e) var event = window.event;
			e.cancelBubble = true;
			if (e.stopPropagation) e.stopPropagation();	
			
			if (Math.abs(k.swipeMove-k.swipeStart) > 30)
			{
				e.preventDefault();
			}
			
			if (k.swipeMove-k.swipeStart < veltijs.movebias) 
			{
				k.noAnim= true;
			}else{
				
				if (k.swipeMove-k.swipeStart >-veltijs.movebias) 
				{
					k.noAnim= true;			
				}			
			}
			
			k.swipeMove = (dir === undefined ? e.targetTouches[0].pageX : e.targetTouches[0].pageY);
			k.pageYOffset2 = window.pageYOffset;
			
			
			if (k.swipeMove-k.swipeStart > veltijs.movebias) 
			{ 		
				if(!veltijs.scrollmove){
				if(Math.abs(k.pageYOffset2 -k.pageYOffset1)<3){
					k.noAnim= false;
					doSwipe(funcA,e); 
				}
				}
			}
			else
			{				
				if (k.swipeMove-k.swipeStart <-veltijs.movebias) 
				{				
					if(!veltijs.scrollmove){
					if(Math.abs(k.pageYOffset2 -k.pageYOffset1)<3){
					k.noAnim= false;
					doSwipe(funcB,e); 
					}
					}
				}
			}
			
					
		};
	}
	else
	{	
		k.onmousedown = function(e)
		{
			e.preventDefault();
			k.swipeStart =
			k.swipeMove  =
			e.pageX;
			k.anim =
			k.capture =
			true;
			e.cancelBubble = true;
			if (e.stopPropagation) e.stopPropagation();
		};
		
		k.onmousemove = function(e)
		{
			if (k.capture)
			{
				if (Math.abs(k.swipeMove-k.swipeStart) > 5)
				{
					e.preventDefault();
				}
				k.swipeMove = e.pageX;
				if (k.swipeMove-k.swipeStart > 30) doSwipe(funcA,e);
				else
				if (k.swipeMove-k.swipeStart <-30) doSwipe(funcB,e);
				e.cancelBubble = true;
				if (e.stopPropagation) e.stopPropagation();		
			}
		};
		
		k.onmouseup   = function(e)
		{
			k.capture = false;
		};
	}
}



veltijs.getParentController = function(controller)
{
	return veltijs.getFirstParentELementByClassName(controller,"pageController");	
}

veltijs.getParentPage = function(page)
{
	return veltijs.getFirstParentELementByClassName(page,"page");
}

veltijs.getFirstParentELementByClassName = function(el,class_name)
{
	var res = [];
	var findme =  function(el){	
		if(el.parentNode!=document.body){			
			if(el.parentNode.className.search(class_name) == -1)
			{
				findme(el.parentNode);
			}else{
				res.push(el.parentNode);			
			}
		}		
	}	
	findme(el);
	return res;
}



veltijs.pagesToDisplay =[];

veltijs.displayPage = function(pagesToGo)
{
		pagesToGo.reverse();
		for(var i=0;i<pagesToGo.length;i++)
		{
			var controller = pagesToGo[i].parentNode;
			if (controller.canTransition) 
			{
				var activeEl = controller.pages[controller.activePage];
				controller.activePage = Array.prototype.slice.call(controller.pages).indexOf(pagesToGo[i]);//$.inArray(pagesToGo[i], controller.pages); //returns the number;
				activeEl.style.display = "none";
				pagesToGo[i].style.display = "block";
				activeEl.className = 'page';
				pagesToGo[i].className = 'page active';
				veltijs.pageTransitionStartListeners(activeEl,pagesToGo[i]);
			}
		}
		veltijs.pagesToDisplay =[];
}


veltijs.getAllPages = function(pageToGo)
{				
		if (pageToGo.isPage) 
		{
			veltijs.pagesToDisplay.push(pageToGo);					
		}	
		if(pageToGo.parentNode!=document.body){			
			veltijs.getAllPages(pageToGo.parentNode);
		}else
		{
			veltijs.displayPage(veltijs.pagesToDisplay);
		}	
}




veltijs.viewPage = function(pagename)
{ 
	
	if(pagename!="")
	{
		var pageToGo = document.getElementById(pagename);
		veltijs.getAllPages(pageToGo);		
	}	
}


veltijs.timersStop = function(carouseTimerCont){
	carouseTimerCont.timerEnabled = false;
	clearInterval(carouseTimerCont.timer_fn);
}

veltijs.timersStart =function(carouseTimerCont){
	carouseTimerCont.timer_fn = setInterval(carouseTimerCont.timer_params.fn,carouseTimerCont.timer_params.time);
	carouseTimerCont.timerEnabled = true;
}

veltijs.stopCarouzelTimers = function(leave_page)
{
	
	var controllers = leave_page.getElementsByClassName('p_controller');
	for(var i=0;i<controllers.length;i++)
	{
		if(controllers[i].getAttribute("rel")=="carouzel"){			
			if(controllers[i].children[0].timer){
				if(controllers[i].children[0].timerEnabled)
				{
					veltijs.timersStop(controllers[i].children[0]);
				}
			}
		};
	}
}

veltijs.startCarouzelTimers = function(enter_page)
{	
	if(enter_page.id!="internal_pages"){ //This is done because the internal pages have the rest of the pages
		var controllers = enter_page.getElementsByClassName('p_controller');
		for(var i=0;i<controllers.length;i++)
		{
		
			if(controllers[i].getAttribute("rel")=="carouzel"){			
				if(controllers[i].children[0].timer){
					if(controllers[i].children[0].timerEnabled==false)
					{
						veltijs.timersStart(controllers[i].children[0]);
					}
				}
			};
		}
	}
}




veltijs.pageTransitionStartListeners = function(leave_page,enter_page)
{
	// This is page Leave and Page Enter Transitions
	//console.log(enter_page.id+'-'+leave_page.id);
	
	veltijs.stopCarouzelTimers(leave_page);
	veltijs.startCarouzelTimers(enter_page);
	
	gorilla.pageTransitionStartListeners(leave_page,enter_page);
	
	
}

veltijs.pageTransitionEndListeners = function(leave_page,enter_page)
{
	// This is page Leave and Page Enter Transitions
	//veltijs.startCarouzelTimers(enter_page);	
	//If this is a carousel start the timers that you have removed on swipe  
	if(leave_page.parentNode.isCarousel){
	if(leave_page.parentNode.timer){
	if(!leave_page.parentNode.timerEnabled){
		veltijs.timersStart(leave_page.parentNode);}
	}
	}
	
	gorilla.pageTransitionEndListeners(leave_page,enter_page);
	
}

veltijs.pageTransitionEnd = function()
{
		this.style.display = 'none';
		this.removeEventListener('webkitAnimationEnd',veltijs.pageTransitionEnd,false);
		this.className = 'page';
		this.nextEl.className = 'page active';
		var controller = this.parentNode;
		controller.activePage = Array.prototype.slice.call(controller.pages).indexOf(this.nextEl);
		controller.canTransition = true;
		veltijs.pageTransitionEndListeners(this,this.nextEl);
		veltijs.showPage = {'type':"noswipe"};
}

veltijs.startPageTransition = function(nextEl,activeEl,dir)
{

	var nextElClass = 'page slidein'; 
	var activeElClass = 'page active slideout';
	
	if(dir=='right'){
		nextElClass = 'page sliderevin';
		activeElClass = 'page active sliderevout';
	}
	nextEl.style.display = 'block';	
	veltijs.pageTransitionStartListeners (activeEl,nextEl);	
	activeEl.addEventListener('webkitAnimationEnd', veltijs.pageTransitionEnd,false);
	nextEl.className = nextElClass;
	activeEl.className = activeElClass;
	activeEl.nextEl = nextEl;
	
							
}

veltijs.showPage = {};


veltijs.swipe = function(controller,direction)
{
	var cont = document.getElementById(controller);

	if (cont.canTransition) 
	{
		setTimeout(function(){
		if(cont.isCarousel!=true){
				scrollTo(0,0);
		}	
		
		var activeEl = cont.pages[cont.activePage];
		if(direction=="left")
		{
		
			if((cont.activePage+1 ==cont.pages.length))
			{			
				//continuous loop  
				if(cont.loop){
					var nextEl = cont.pages[0];
					cont.activePage = 0;
					cont.canTransition = false;	
					if(cont.isCarousel)	{
						if(cont.timerEnabled){
						veltijs.timersStop(cont);}
						veltijs.startPageTransition(nextEl,activeEl);
					}
					else
					{					
					veltijs.showPage = { 'type':"swipe",
										 'activeEl':activeEl,
										 'nextEl':nextEl,
										 'direction':"left"
										};
						location.hash = nextEl.id;
					}
				}
			}
			else{			
				var nextEl = cont.pages[cont.activePage+1];
				cont.activePage = cont.activePage+1;
				cont.canTransition = false;
				if(cont.isCarousel){
					if(cont.timerEnabled){
						veltijs.timersStop(cont);}
					veltijs.startPageTransition(nextEl,activeEl,direction);
				}
				else{
				veltijs.showPage = { 'type':"swipe",
									 'activeEl':activeEl,
									 'nextEl':nextEl,
									 'direction':direction
									};
				location.hash = nextEl.id;
				}	
			}
			
		}else
		{		
				if((cont.activePage-1 < 0) )
				{			
					if(cont.loop){
						var nextEl = cont.pages[cont.pages.length-1];
						cont.activePage = cont.pages.length-1;
						cont.canTransition = false;	
						if(cont.isCarousel){		
							if(cont.timerEnabled){
								veltijs.timersStop(cont);}
							veltijs.startPageTransition(nextEl,activeEl,direction);
						}
						else{
							veltijs.showPage = { 'type':"swipe",
											 'activeEl':activeEl,
											 'nextEl':nextEl,
											 'direction':direction
											};
							location.hash = nextEl.id;
						}			
					}
				}
				else{			
					var nextEl = cont.pages[cont.activePage-1];
					cont.activePage = cont.activePage-1;
					cont.canTransition = false;
					if(cont.isCarousel){
						if(cont.timerEnabled){
							veltijs.timersStop(cont);}
						veltijs.startPageTransition(nextEl,activeEl,direction);
					}
					else{
					veltijs.showPage = { 'type':"swipe",
										 'activeEl':activeEl,
										 'nextEl':nextEl,
										 'direction':direction
										};
					location.hash = nextEl.id;
					}
				}	
		}
		
		if(cont.dotsEnabled)
		{
			var dotsCont = document.getElementById(cont.id+"_dotsCont");
			for(var d=0;d<dotsCont.children.length;d++)
			{
				//alert(d)
				document.getElementById(cont.id+"_dot_"+d).style.background="white";
			}
			document.getElementById(cont.id+"_dot_"+cont.activePage).style.background="#009ed3";
		}
		
		},50);
	}
	
	
}
veltijs.pageTouchSwipe = function(e)
{
	var swipedElement = document.getElementById(veltijs.triggerElementID);	
	if ( veltijs.swipeDirection == 'left' ) {
		veltijs.swipe(swipedElement.id,'left');		
	} else if ( veltijs.swipeDirection == 'right' ) {	
		veltijs.swipe(swipedElement.id,'right'); 
	} else if ( veltijs.swipeDirection == 'up' ) {	
	
	} else if ( veltijs.swipeDirection == 'down' ) {
	
	}
	
}



veltijs.locationchangeView = function(pageToGo)
{
	//alert(pageToGo+'1111')
	veltijs.imagePreloading();	
	if(veltijs.showPage.type=='swipe')
	{
	    veltijs.startPageTransition(veltijs.showPage.nextEl,veltijs.showPage.activeEl,veltijs.showPage.direction);
	}else{
		veltijs.viewPage(pageToGo);
	}
}

veltijs.onlocationchange = function()
{
	var hashVal = location.hash;
	var pageToGo = hashVal.replace('#','');	
	veltijs.locationchangeView(pageToGo);
}





veltijs.imagesPreloaded = false;
veltijs.imagePreloading = function(/*cont*/)
{
	
	if(!veltijs.imagesPreloaded){
		var cont =arguments[0]||document;
		var images = [];
		var imgs = cont.getElementsByTagName('img');
		var alldiv = cont.getElementsByTagName('div');	
		for(var d=0;d<imgs.length;d++)
		{
			var extImg = imgs[d].getAttribute("rel")
			if(extImg)
			{
				//Preloading external images
				if(extImg == "external")
				{
					if(imgs[d].src.search("USER_AGENT") >-1)
					{
						var exturl = imgs[d].src.split("$")[0] + USER_AGENT;
						images.push(exturl);
					}else
					{
						images.push(imgs[d].src);
					}
					
				}
			}
			else
			{
				images.push(imgs[d].src.match(/(assets\/.*?(png|gif|jpg))/g));
			}
		
		}	
		
		for(var i=0;i<alldiv.length;i++)
		{
			if(alldiv[i].style.background!="")
			{
				if(alldiv[i].style.cssText.indexOf('assets') >-1)
				{
					var elmatch = alldiv[i].style.cssText.match(/(assets\/.*?(png|gif|jpg))/g);	
					images.push(elmatch);					
				}
			}		
		}
		
			var pre = function(num)
			{
				
				
				veltijs.setloadingBar(num,images);
				if(num < images.length)
				{
					var img = new Image();
				    img.src = images[num];
				    img.onload = function(){			    	
				    		pre(num+1);
				    };
				   
				}else{
					console.log("--------------------------------*******************____________________________________")
			    	veltijs.imagesPreloaded = true;
			    	//setTimeout(function()
					//{
						document.getElementById('loader').style.display ="none";
						document.getElementById('velti_box').style.display ="block";
						gorilla.addScroll2('menu_wrapper');
						
					//}, 2000);
					//gorilla.iscrollscrollers["menu_wrapper"].refresh();
					if(gorilla.isExternal!=false)
					{
						console.log('=====external script run--');
						gorilla.externalPageRunScripts(gorilla.isExternal);
						
					}		    	
			    	
			    }
			}
			pre(0);	
		}

}


veltijs.initialTimer = function(pageContainer)
{
	if(pageContainer.style.display!='none')
		if(pageContainer.canTransition ==true);
			veltijs.swipe(pageContainer.id,'left');
	
}

veltijs.newPageControllerParser = function(controlName,loop,activePageNum,swipeEnabled,dotsEnabled,timer)
{
	
	var pageContainer = document.getElementById(controlName);
	pageContainer.isCarousel = false;
	if(pageContainer.parentNode.getAttribute("rel")=="carouzel")
	{pageContainer.isCarousel = true;}
	pageContainer.className ="pageController";
	pageContainer.isController = true;
	pageContainer.timer_params = {"fn":function(){veltijs.initialTimer(pageContainer)},"time":5000};
	if(swipeEnabled){
	veltijs.addSwipe(controlName, 
		function(){veltijs.swipe(controlName,'right')},
		function(){veltijs.swipe(controlName,'left')});
	}
	if(timer)
	{
		pageContainer.timer = true;
		pageContainer.timerEnabled = false;
		pageContainer.timer_fn = "";							
	}	
	pageContainer.canTransition =true;
	pageContainer.loop = loop;
	pageContainer.dotsEnabled = dotsEnabled;
	if(dotsEnabled)
	{
		var dotsCont = document.createElement('div');
		dotsCont.id = controlName+"_dotsCont";
		dotsCont.className ="hbox center_box";
		dotsCont.style.width="100%";
		pageContainer.parentNode.appendChild(dotsCont);
	}
	pageContainer.pages = pageContainer.children;//$('#'+controlName).children();	
	for(var i=0;i<pageContainer.pages.length;i++)
	{		
		
		pageContainer.pages[i].className ="page";
		pageContainer.pages[i].isPage = true;
		pageContainer.pages[i].controller = pageContainer;
		if(i==activePageNum)
		{		
			pageContainer.pages[i].className = pageContainer.pages[i].className + " active";
			pageContainer.activePage = activePageNum;
		}
		if(dotsEnabled)
		{			
			var dots = document.createElement('div');
			dots.id=controlName+"_dot_"+i
			dots.style.cssText="margin-left:3px; margin-right:3px; margin-top:15px; border:1px solid #009ed3; -moz-border-radius:35px; border-radius: 35px; height:4px; width:4px; background:white";
			if(i==0)
			dots.style.cssText="margin-left:3px; margin-right:3px; margin-top:15px; border:1px solid #009ed3; -moz-border-radius:35px; border-radius: 35px; height:4px; width:4px; background:#009ed3";
			
			dotsCont.appendChild(dots);
			
		}	
	}
	pageContainer.parentController =  veltijs.getParentController(pageContainer);
	pageContainer.parentPage  = veltijs.getParentPage(pageContainer);
	
	//return pageContainer;
}



document.addEventListener('DOMContentLoaded',function(){
	veltijs.createloadingBar();
	window.addEventListener('scroll', function(){
		veltijs.scrollmove = true;
		
	}, false);
	USER_AGENT = window.navigator.userAgent;
	gorilla.createPageControllers();},false
	);

window.onorientationchange = function(){
    veltijs.deviceOrientation  = veltijs.getOrientation();
    gorilla.orientationChangeListener(veltijs.deviceOrientation);
};


veltijs.getOrientation = function(){
	var o = window.orientation;
    //alert(navigator.userAgent); //gt-p7\d+
    if(navigator.userAgent.toLowerCase().search(/gt-p7|gt-p7510|a100|tf101g|thinkpad|mz601|xoom/) > -1)
	{
		var res = (o === 0 || o === 180)? 'l':'p';
	}else{
		var res = (o === 0 || o === 180)? 'p':'l';
	}
	
	return res
};

//AJAX
veltijs.XHRPool = (
		function() {
			var stack = [];
			var poolSize = 10;
			var nullFunction = function(){};
			function createXHR() {
				if (window.XMLHttpRequest)
					return new XMLHttpRequest();
				else if (window.ActiveXObject)
					return new ActiveXObject('Microsoft.XMLHTTP');
	            else
	                return null;
			}
			for (var i = 0; i<poolSize; i++)
				stack.push(createXHR());
			return ({
				release : function(xhr) {
					xhr.onreadystatechange = nullFunction;
					xhr.abort();
					stack.push(xhr);
				},
				getInstance : function() {
					if (stack.length < 1)
		    			return createXHR();
					else
						return stack.pop();
		  		},
		  		toString : function() {
					return "stack size = " + stack.length;
				}
		 	});
		}
	)();

veltijs.proxy = {};
veltijs.proxy.ENDPOINT = 'proxy.php';
veltijs.proxy.TYPE_JSON = 'application/json';
veltijs.proxy.TYPE_XML = 'text/xml';
veltijs.proxy.TYPE_FORM_POST = 'application/x-www-form-urlencoded';

veltijs.proxy.encodeData = function(attrs) {
    var lst = [];
    for (var key in attrs) {
        var k = key + "=" + encodeURIComponent(attrs[key]);
        lst.push(k)
    }
    return lst.join('&');
}

veltijs.proxy.sendRequest = function(url, httpMethod, async /*, data, oncomplete, headers */) {
    
	var headers = arguments[5] || {},
		data = arguments[3] || false,
        oncomplete = arguments[4] || false,
        http = veltijs.XHRPool.getInstance(),
        httpMethod = httpMethod.toUpperCase(),
        payload = null,
        endpoint = veltijs.proxy.ENDPOINT;

    if (httpMethod == 'POST') {
        if (!headers['Content-Type']) {
            throw 'No Content-Type header specified';
        }
        switch (headers['Content-Type']) {
            case veltijs.proxy.TYPE_JSON:
                data = JSON.stringify(data);
                break;
            case veltijs.proxy.TYPE_FORM_POST:
                data = veltijs.proxy.encodeData(data);
                break;
        }
        payload = JSON.stringify({
            url : url,
            data : data,
            headers : headers
        });
        http.open(httpMethod, endpoint, async);
        http.setRequestHeader("Content-Type", "application/json");
    }
    else {
        if (data) {
            data = veltijs.proxy.encodeData(data);
            endpoint = endpoint + "?url=" + encodeURIComponent(url + "?" + data);
        }
        else {
            endpoint = endpoint + "?url=" + encodeURIComponent(url);
        }
        http.open(httpMethod, endpoint, async);
    }

    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            try{
                if(oncomplete) {
                    oncomplete(http);
                }
            }
            finally {
            	veltijs.XHRPool.release(http);
            }
        }
    }
    http.send(payload);
}



veltijs.sendRequest = function(url, httpMethod, async /*, data, oncomplete, headers */) {
    
	var headers = arguments[5] || {},
		data = arguments[3] || false,
        oncomplete = arguments[4] || false,
        http = veltijs.XHRPool.getInstance(),
        httpMethod = httpMethod.toUpperCase(),
        payload = null;

    if (httpMethod == 'POST') {
        if (!headers['Content-Type']) {
            throw 'No Content-Type header specified';
        }
        switch (headers['Content-Type']) {
            case veltijs.proxy.TYPE_JSON:
                data = JSON.stringify(data);
                break;
            case veltijs.proxy.TYPE_FORM_POST:
                data = veltijs.proxy.encodeData(data);
                break;
        }
        payload = data ;
        endpoint = encodeURIComponent(url);
        http.open(httpMethod, endpoint, async);
        http.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    }
    else {
        if (data) {
            data = veltijs.proxy.encodeData(data);
            endpoint = encodeURIComponent(url + "?" + data);
        }
        else {
            endpoint = url;
        }
        http.open(httpMethod, endpoint, async);
    }

    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            try{
                if(oncomplete) {
                    oncomplete(http);
                }
            }
            finally {
            	veltijs.XHRPool.release(http);
            }
        }
    }
    http.send(payload);
}


