var gorilla = {};

gorilla._init_ = function()
{
	if (location.hash == "")
	{
		location.hash = "home_page";
		veltijs.locationchangeView('home_page');
	}
	else
	{
		var pageToGo = location.hash.replace('#','');	
		veltijs.locationchangeView(pageToGo);
	}
	gorilla.preloadFunWallpapers();
	//console.log("finishing body.onload**************************************************");
	//gorilla.cube();
};

gorilla.hovereffect = function(el,state,btn)
{
	if (window.navigator.userAgent.toLowerCase().search('blackberry') == -1){
		if(btn){
			(state)?el.style.color="white":el.style.color="#009ED3";
		}
		else{
		(state)?el.style.background="#cccccc":el.style.background="none";}
	} 
}

gorilla.showWallpaper = function(n, end)
{
	scroll(0, 0);
	
	if (end !== undefined)
	{
		document.getElementById('showWallpaper').style.display = 'none';
		wallpaper_selected_item = -1;
		return;
	}
	document.getElementById('wallpaperSource').src = 'assets/funWithGorillaPage/carousel/a'+n+'.jpg';
	document.getElementById('showWallpaper').style.display = 'block';
	document.getElementById('showWallpaper').ontouchmove = function(e)
	{
		//e.cancelBubble = true;
		//e.preventDefault();
	}
	wallpaper_selected_item = n;		
	
}

var carousels2d;

gorilla.buildCarousels2d = function()
{
	carousels2d = document.querySelectorAll('.carousel2d');
	
	for (var d=0; d<carousels2d.length; d++)
	{
		var
		childs = carousels2d[d].childNodes[1].childNodes;
		carousels2d[d].childs = [];
		carousels2d[d].position = 0;
		
		for (var k=0; k<childs.length; k++)
		{
			if (childs[k].nodeType === 1)
			{
				carousels2d[d].childs.push(childs[k]);
			}
		}
		
		veltijs.addSwipe(carousels2d[d].id,
		function(e, k)
		{
			if (k.position+1 === 1) return;
			k.childNodes[1].style.webkitTransform = 'translateX('+(++k.position*270)+'px)';
			
			switch(videos_current_share)
			{
			case 'victor':
				videos_selected_item_1 = Math.abs(k.position);
				break;
				
			case 'demos':
				videos_selected_item_2 = Math.abs(k.position);
				break;
				
			case 'saying':
				videos_selected_item_3 = Math.abs(k.position);
				break;
				
			case 'brought':
				videos_selected_item_4 = Math.abs(k.position);
				break;
			}
			
			var	whichLetter = e.parentNode.parentNode.id[0];
			console.log(whichLetter+'_videos_carousel2d_text ---> assets/videosPage/'+whichLetter+Math.abs(k.position)+'.png');
			document.getElementById(whichLetter+'_videos_carousel2d_text').src = 'assets/videosPage/'+whichLetter+Math.abs(k.position)+'.png';
		},
		function(e, k)
		{
			if (k.position-1 === -k.childs.length) return;
			k.childNodes[1].style.webkitTransform = 'translateX('+(--k.position*270)+'px)';
			
			switch(videos_current_share)
			{
			case 'victor':
				videos_selected_item_1 = Math.abs(k.position);
				break;
				
			case 'demos':
				videos_selected_item_2 = Math.abs(k.position);
				break;
				
			case 'saying':
				videos_selected_item_3 = Math.abs(k.position);
				break;
				
			case 'brought':
				videos_selected_item_4 = Math.abs(k.position);
				break;
			}
			
			var	whichLetter = e.parentNode.parentNode.id[0];
			console.log(whichLetter+'_videos_carousel2d_text ---> assets/videosPage/'+whichLetter+Math.abs(k.position)+'.png');
			document.getElementById(whichLetter+'_videos_carousel2d_text').src = 'assets/videosPage/'+whichLetter+Math.abs(k.position)+'.png';
		});
	}
};

var carousels3d;

gorilla.buildCarousels3d = function()
{
	carousels3d = document.querySelectorAll('.carousel3d');
	
	for (var d=0; d<carousels3d.length; d++)
	{
		var
		childs = carousels3d[d].childNodes[1].childNodes;
		carousels3d[d].childs = [];
		carousels3d[d].position = -1;
		carousels3d[d].childNodes[1].style.webkitTransform = 'translateX(-100px)';
		
		for (var k=0; k<childs.length; k++)
		{
			if (childs[k].nodeType === 1)
			{
				carousels3d[d].childs.push(childs[k]);
			}
		}
		
		veltijs.addSwipe(carousels3d[d].id,
		function(e, k)
		{
			if (k.position+1 === 1) return;
			k.childNodes[1].style.webkitTransform = 'translateX('+(++k.position*100)+'px)';
			
			if (Math.abs(k.position)-1 !== -1) k.childs[Math.abs(k.position)-1].style.webkitTransform = 'rotateY(50deg)';
			k.childs[Math.abs(k.position)+0].style.webkitTransform = 'scale(1.6)';
			if (Math.abs(k.position)+1 !== k.childs.length) k.childs[Math.abs(k.position)+1].style.webkitTransform = 'rotateY(-50deg)';
			
			if (Math.abs(k.position)-1 !== -1) k.childs[Math.abs(k.position)-1].style.opacity = .7;
			k.childs[Math.abs(k.position)+0].style.opacity = 1;
			if (Math.abs(k.position)+1 !== k.childs.length) k.childs[Math.abs(k.position)+1].style.opacity = .7;
		},
		function(e, k)
		{
			if (k.position-1 === -k.childs.length) return;
			k.childNodes[1].style.webkitTransform = 'translateX('+(--k.position*100)+'px)';
			
			if (Math.abs(k.position)-1 !== -1) k.childs[Math.abs(k.position)-1].style.webkitTransform = 'rotateY(50deg)';
			k.childs[Math.abs(k.position)+0].style.webkitTransform = 'scale(1.6)';
			if (Math.abs(k.position)+1 !== k.childs.length) k.childs[Math.abs(k.position)+1].style.webkitTransform = 'rotateY(-50deg)';
			
			if (Math.abs(k.position)-1 !== -1) k.childs[Math.abs(k.position)-1].style.opacity = .7;
			k.childs[Math.abs(k.position)+0].style.opacity = 1;
			if (Math.abs(k.position)+1 !== k.childs.length) k.childs[Math.abs(k.position)+1].style.opacity = .7;
			
		});
	}
};

gorilla.makeAccordeons = function()
{
	function getComputedHeight(e)
	{
		return parseInt(window.getComputedStyle(e, null).getPropertyValue('height'));
	}
	
	var
	accordeons = document.querySelectorAll('.accordeon');
	
	window.addEventListener('orientationchange',
	function()
	{
		for (var d=0; d<accordeons.length; d++)
		{
			for (var k=0; k<accordeons[d].childNodes.length; k++)
			{
				if (accordeons[d].childNodes[k].className === 'accordeon-content')
				{
					setTimeout(function(actualD, actualK)
					{
						return function()
						{
							var
							scratch = accordeons[actualD].childNodes[actualK].parentNode.childNodes[1];
							
							if (getComputedHeight(accordeons[actualD].childNodes[actualK]))
							{
								scratch.innerHTML= accordeons[actualD].childNodes[actualK].innerHTML;
								accordeons[actualD].childNodes[actualK].style.height = getComputedHeight(scratch);
							}
						};
					}(d, k), 250);
				}
			}
		}
	},
	false);
	
	for (var d=0; d<accordeons.length; d++)
	{
		var
		childs = accordeons[d].childNodes;
		
		for (var k=0; k<childs.length; k++)
		{
			if (childs[k].className === 'accordeon-caption')
			{
				childs[k].onclick = function(actualD, actualK)
				{
					return function()
					{
						var
						childs2 = accordeons[actualD].childNodes;
						
						if (actualD !== 0)
						{
							for (var h=0; h<childs2.length; h++)
							{
								if (childs2[h].className === 'accordeon-content')
								{
									childs2[h].style.height = 0;
								}
								else
								if (childs2[h].className === 'accordeon-caption')
								{
									childs2[h].style.background = 'url(\'assets/acc-normal.png\')';
									childs2[h].style.color = '#009ed3';
									childs2[h].childNodes[1].innerText = '+';
								}
							}
						}
						
						var
						div     = this.nextSibling.nextSibling,
						scratch = this.parentNode.childNodes[1];
						
						if (!getComputedHeight(div))
						{
							scratch.innerHTML= div.innerHTML;
							div.style.height = getComputedHeight(scratch);
							scratch.innerHTML= '';
							this.style.background = 'url(\'assets/acc-expanded.png\')';
							this.childNodes[1].innerHTML = '&mdash;';
							this.style.color = '#fff';
							
							switch(accordeons[actualD].id)
							{
							case 'accordeon1':
								switch(actualK)
								{
								case  3:
									innovate_current_share = 'overview';
									break;
									
								case  7:
									innovate_current_share = 'characteristics';
									break;
									
								case 11:
									innovate_current_share = 'customization';
									break;
									
								case 15:
									innovate_current_share = 'applications';
									break;
									
								case 19:
									innovate_current_share = 'literature';
									break;
								}
								break;
								
							case 'accordeon2':
								switch(actualK)
								{
								case  3:
									videos_current_share = 'victor';
									break;
									
								case  7:
									videos_current_share = 'demos';
									break;
									
								case 11:
									videos_current_share = 'saying';
									break;
									
								case 15:
									videos_current_share = 'brought';
									break;
								}
								break;
							}
						}
						else
						{
							div.style.height = 0;
							this.style.background = 'url(\'assets/acc-normal.png\')';
							this.childNodes[1].innerText = '+';
							this.style.color = '#009ed3';
						}
					};
				}(d, k);
			}
		}
	}
}

gorilla.orientationChangeListener = function(o)
{
	var str = location.hash;
	var page = str.replace("#","");	
	gorilla.iscrollscrollers["menu_wrapper"].refresh();
}

gorilla.setServices = (function()
{ 			    
    // set services urls for staging and live envs 
    var objServices = [{
        press_release_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/press_release',
        news_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/news',
        events_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/event',
        showall_press_release_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/press_release',
        showall_news_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/news',
        showall_events_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/event',
        sendfile_viaemail_service:'http://mgage-us-staging.velti.com/cms/davinci/page/testStav/@dev/page1',
        sendfile_viaemail_host:'http://mgage-us-staging.velti.com/cms/plugins/com/velti/SendToFriendViaEmail/backEnd/SendEmailService',
        facebook_url:'http://gorilla-stag.v4en.com/fb/share/share.jsp?p=',
        faq_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/faqs',
        media_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/media',
        consumers_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/consumers',
        submit_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/comment',
        sms_service:'http://10.64.102.210:8080/message-toolkit/messagerequests'
    },
    {
        press_release_service:'http://gorilla.v4en.com/rssReader/gorilla/press_release',
        news_service:'http://gorilla.v4en.com/rssReader/gorilla/news',
        events_service:'http://gorilla.v4en.com/rssReader/gorilla/event',
        showall_press_release_service:'http://gorilla.v4en.com/rssReader/gorilla/press_release',
        showall_news_service:'http://gorilla.v4en.com/rssReader/gorilla/news',
        showall_events_service:'http://gorilla.v4en.com/rssReader/gorilla/event',
        sendfile_viaemail_service:'http://mgage-us.velti.com/cms/davinci/page/testStav/@dev/page1',
        sendfile_viaemail_host:'http://mgage-us.velti.com/cms/plugins/com/velti/SendToFriendViaEmail/backEnd/SendEmailService',
        facebook_url:'http://m.corninggorillaglass.com/fb/share/share.jsp?p=',
        faq_service:'http://gorilla.v4en.com/rssReader/gorilla/faqs',
        media_service:'http://gorilla.v4en.com/rssReader/gorilla/media',
        consumers_service:'http://gorilla.v4en.com/rssReader/gorilla/consumers',
        submit_service:'http://gorilla.v4en.com/rssReader/gorilla/comment',
        sms_service:'http://10.64.102.201:8888/message-toolkit/messagerequests'
    }];
    
    if (document.location.href.search('gorilla-stag.v4en') > -1)
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    else if ((document.location.href.search('corninggorillaglass') > -1) || (document.location.href.search('gorilla.v4en') > -1) )
    {
        //live url
    	gorilla.services=objServices[1];
    }
    else if (document.location.href.search('localhost') > -1)
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    else
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    
})();

gorilla.showPageById = function(id)
{	
	veltijs.showPage = {'type':"noswipe"};
	location.hash = id;	
};

var cubeTimer    = null,
	cubeRestart  = null,
	cubeRotation = 0;

gorilla.cubeStartTimer = function()
{
	if (window.navigator.userAgent.toLowerCase().search('android') > -1)
	{
	}
	else
	{
		cubeTimer = setInterval(function()
		{
			document.getElementById('home-cube-axis').style.webkitTransform = 'rotateY('+(cubeRotation-=90)+'DEG)';
		},
		4000);
	}
};

gorilla.cubeStopTimer = function()
{
	if (window.navigator.userAgent.toLowerCase().search('android') > -1 || window.navigator.userAgent.toLowerCase().search('blackberry') > -1)
	{
	}
	else
	{
		clearInterval(cubeTimer);
		clearTimeout (cubeRestart);
		
		cubeRestart = setTimeout(function()
		{
			gorilla.cubeStartTimer();
		},
		4000);
	}
};

gorilla.createPageControllers = function()
{
	veltijs.analytics();
	veltijs.setAnalyticsEvents(document);
	
	var pageControllers = document.getElementsByClassName('p_controller');
	for (var i=0;i<pageControllers.length;i++)
	{
		var dotsEnabled = false;
		var timer = false;
		var loop = false;
		var swipeEnabled = true;
	 	if(pageControllers[i].getAttribute("rel") == "carouzel")
	 	{
			//if this  controller is a carouzel
	 		var dotsEnabled = true;
			var timer = true;
			var loop = true;
			if(pageControllers[i].children[0].id=="products_contentCarouzel")
			{
				var dotsEnabled = false;
				var timer = true;
				var loop = true;
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="home-2d-cube-scroller")
			{
				var dotsEnabled = true;
				var timer = true;
				var loop = true;
				var swipeEnabled = true;
			}
			if(pageControllers[i].children[0].id=="home-4d-cube-scroller")
			{
				var dotsEnabled = true;
				var timer = false;
				var loop = false;
				var swipeEnabled = true;
			}
		}
	 	else
	 	{
			//if this  controller is page
			if (pageControllers[i].children[0].id=="pages_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="main_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="ext_pages_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="innovation_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="video_Controller")
			{
				var swipeEnabled = false;
			}	
		}
		veltijs.newPageControllerParser(pageControllers[i].children[0].id,loop,0,swipeEnabled,dotsEnabled,timer);
	}
	window.addEventListener("hashchange", veltijs.onlocationchange, false);
	
	gorilla.makeAccordeons();
	//Select between 2d or 3d mode, depending on the target device.
	if (window.navigator.userAgent.toLowerCase().search('android') > -1 || window.navigator.userAgent.toLowerCase().search('blackberry') > -1)
	{
		document.getElementById('home-cube').style.display = 'none';
	}
	else
	{
		document.getElementById('home-2d-cube').style.display = 'none';
		
		veltijs.addSwipe('home-cube',
		function()
		{
			document.getElementById('home-cube-axis').style.webkitTransform = 'rotateY('+(cubeRotation+=90)+'DEG)';
			gorilla.cubeStopTimer();
		},
		function()
		{
			document.getElementById('home-cube-axis').style.webkitTransform = 'rotateY('+(cubeRotation-=90)+'DEG)';
			gorilla.cubeStopTimer();
		});
	}
	
	//Select between 2d or 3d mode, depending on the target device.
	if (window.navigator.userAgent.toLowerCase().search('android') > -1 || window.navigator.userAgent.toLowerCase().search('blackberry') > -1)
	{
		document.getElementById('fun_carousel3d').style.display = 'none';
	}
	else
	{
		document.getElementById('home-4d-cube').style.display = 'none';
	}
	
	gorilla.buildCarousels2d();
	gorilla.buildCarousels3d();
	gorilla.cubeStartTimer();
	
	setTimeout(function()
	{
		var	doc = document.documentElement;
		
		doc.style.height = 10000;
		scrollTo(0, 0);
		doc.style.height = window.innerHeight;
	},
	250);
};

gorilla.displaySwipeBtns = function(disp)
{
	var swipe_btn_left = document.getElementById("swipe_btn_left");
	var swipe_btn_right = document.getElementById("swipe_btn_right");
	if(disp)
	{
		swipe_btn_left.style.display="block";
		swipe_btn_right.style.display="block";
	}
	else
	{
		swipe_btn_left.style.display="none";
		swipe_btn_right.style.display="none";
	}
};

gorilla.productPages = [
		         "Acer_page","Asus_page","Dell_page",
		         "Htc_page","Lg_page","Motorola_page",
		         "Nec_page","Nokia_page","Samsung_page",
		         "SK_Telesys_page","Sonim_page","Lenovo_page",
		         "Motion_Computing_page","Hyundai_page","Sony_page",
		         "HP_page","AboutUs_page","ContactUs_page",
		         "ProductsFull_page","SamsungSUR40_page","Lumigon_page"];

gorilla.extPages=['SpecificNewsAndFeeds_page',
                  'AllNewsAndFeeds_page',
                  'GorillaEmail_page',
                  'GorillaSMS_page',
                  'AllPraise_page',
                  'PraiseThankYou_page',
                  'WallpaperPreview_page',
                  'VideoPreview_page'];

gorilla.isExternal = false;

gorilla.pageTransitionStartListeners = function(leave_page,enter_page)
{	
	var o = veltijs.getOrientation();
	if(enter_page.parentNode.isCarousel)
	{	
		// This is a carousel
	}
	else
	{
		gorilla.isExternal = false;
		if((enter_page.id!='internal_pages')&&(enter_page.id!='external_pages')){
			veltijs.analyticsTrackPage(enter_page);
		}
		
		if(enter_page.parentNode.id == 'pages_Controller')
		{
			gorilla.changeMainMenuBtnsStyle(enter_page.id.replace('page','menu_btn'));
		}
		
		if(gorilla.productPages.indexOf(enter_page.id)>-1)
		{
			gorilla.isExternal = enter_page;
			
			var ext_page = 'product_pages/'+enter_page.id+'.html';
			veltijs.sendRequest(ext_page, 'GET',true, null, gorilla.externalPageParse);
		}
		
		if(gorilla.extPages.indexOf(enter_page.id)>-1)
		{
		}
		
		switch(enter_page.id)
		{
		case 'home_page':
		case 'products_page':
		case 'innovating_page':
			break;
			
		case 'news_page':
			if (!gorilla.newsEventsCreated) gorilla.getNewsAndEventsPage();
			break;
			
		case 'AllNewsAndFeeds_page':
			scrollTo(0,0);
			break;
			
		case 'SpecificNewsAndFeeds_page':
			var tableList = document.getElementsByTagName('table');
			if(tableList.length)
			{
				for(var i=0;i<tableList.length;i++)
				{
					tableList[i].style.width="100%";
				} 
			}
			
			scrollTo(0,0);
			break;	
			
		case 'fun_page':
			if (window.navigator.userAgent.toLowerCase().search('version/4.') > -1)
			{		
				var audio = document.getElementById("gorilla_ringtones");
				audio.style.display="none";					
			}
			break;
			
		case 'faqs_page':
			if (!gorilla.faqCreated) gorilla.getFaqPage();
			break;
			
		case 'news_page':
			if (!gorilla.newsEventsCreated)
				gorilla.getNewsAndEventsPage();
			break;	
				
		case 'faqs_page':
			if (!gorilla.faqCreated)
				gorilla.getFaqPage();
			break;
				
		case 'praise_page':
			var validationPraiseField = document.getElementById('validation_praise');
			validationPraiseField.innerHTML = "";
			if (!gorilla.praiseCreated){			
					gorilla.getPraisePage()
			};
			break;	
		case 'GorillaEmail_page':
			gorilla.clearEmaillabel();
			break;	
		case 'GorillaSMS_page':				
			gorilla.clearSmslabel();
			break;	
				
		case 'praise_page':
			var validationPraiseField = document.getElementById('validation_praise');
			validationPraiseField.innerHTML = "";
			if (!gorilla.praiseCreated)
			{
				gorilla.getPraisePage()
			};
			break;
		case 'About_page':
			gorilla.switchLoadingIndicator(false);
			break;
		case 'Contact_page':
			gorilla.switchLoadingIndicator(false);
			break;
		}
		switch(leave_page.id)
		{
		case 'home_page':
		case 'products_page':
		case 'videos_page':
		case 'victor_page':
		case 'demos_page':
		case 'saying_page':
		case 'brought_page':
			break;
		}
	}
};

gorilla.pageTransitionEndListeners = function(leave_page,enter_page)
{
	if(enter_page.parentNode.isCarousel)
	{
		// this is a carousel not a page
	}
	else
	{
		
		//This is a page
		switch (enter_page.id)
		{
			case 'home_page':
				break;
			case 'products_page':
				break;
			case 'innovating_page':
				break;
		
		}
		
		switch (leave_page.id)
		{
			
			case 'home_page':
				break;
			case 'products_page':
				break;
		}
	}
};

gorilla.changeMainMenuBtns = function(pageToGoBtn)
{	
	var currentPageId = location.hash.replace("#",'');
	var currentPage = document.getElementById(currentPageId);
	var pageToGoId = pageToGoBtn.id.replace('menu_btn','page');
	
	
	if(currentPageId!=pageToGoId){
		console.log(currentPage.controller.canTransition);
		if(currentPage.controller.canTransition == true){
			gorilla.changeMainMenuBtnsStyle(pageToGoBtn.id);
			if(currentPage.controller.id!="ext_pages_Controller"){
				currentPage.controller.canTransition = false;			
				gorilla.gotToPage(pageToGoBtn);
			}else{
				gorilla.showPageById(pageToGoId);
			}
		}
	}
};

gorilla.gotToPage = function(pageToGo)
{
	var pagename = pageToGo.id.replace('menu_btn','page');	
	var el = document.getElementById(location.hash.replace("#",''));
	veltijs.showPage = { 'type':"swipe",
				 'activeEl':el,
				 'nextEl':document.getElementById(pagename),
				 'direction':"left"
		};	
	
	location.hash=pagename;
};

gorilla.changeMainMenuBtnsStyle = function(menuname)
{
	var item = document.getElementById(menuname);
	if(item!=null){
		var positionObj = {
					products_menu_btn:"-5px", 
					innovating_menu_btn:"-85px", 
					news_menu_btn:"-172px", 
					videos_menu_btn:"-287px", 
					fun_menu_btn:"-345px", 
					faqs_menu_btn:"-475px",
					praise_menu_btn:"-523px"
				}	        
		var menu_cont = document.getElementById("thelist");
		for (var i=0; i<menu_cont.children.length; i++)
		{
			menu_cont.children[i].children[0].style.backgroundPosition = positionObj[Object.keys(positionObj)[i]] + ' -1px';
		}
		if (menuname !== 'home_menu_btn') item.style.backgroundPosition= positionObj[menuname] +" -31px";
		
		gorilla.showWallpaper(0, 0);
	}
};



//------------------|Accessing External Pages|--------------------//
gorilla.openExternalPage = function(el)
{
	var pagename = el.id.replace('btn','page');
	gorilla.switchLoadingIndicator(true);
	gorilla.showPageById(pagename);
};

gorilla.switchLoadingIndicator = function(sw)
{
	scrollTo(0,0);
	var el = document.getElementById('loadingIndicator');
	el.ontouchmove = function(e)
	{
		e.cancelBubble = true;
		e.preventDefault();
	}
	
	if(sw){
		el.style.height = window.innerHeight;
		el.style.top = 0;
		el.style.display = "block";		
	}
	else{
	   el.style.display="none";
	}
};

gorilla.externalPageRunScripts = function(el)
{
	var scripts  = el.getElementsByTagName('script');

	if(scripts.length>0){
		for(var i=0; i<scripts.length; i++)
		{
			eval(scripts[i].innerHTML);
		}
	}
}

gorilla.externalPageParse = function(req)
{
	
	var el = document.getElementById(location.hash.replace("#",''));
	el.innerHTML = req.responseText;
	//gorilla.switchLoadingIndicator(false);
	veltijs.setAnalyticsEvents(el);
	
	if(veltijs.imagesPreloaded){
		console.log("running ext scripts from within")
		gorilla.switchLoadingIndicator(false);
		gorilla.externalPageRunScripts(el);
	}
	
};
//------------------|Accessing External Pages END|--------------------//



//---------------------|News and Events Page|-------------------------//
gorilla.newsEventsCreated = false;
 
gorilla.getNewsAndEventsPage = function()
{
	veltijs.proxy.sendRequest(gorilla.services.press_release_service, 'GET',true, null, gorilla.newsReleasesParse);
	veltijs.proxy.sendRequest(gorilla.services.news_service, 'GET', true, null, gorilla.inThenewsParse);
	veltijs.proxy.sendRequest(gorilla.services.events_service, 'GET', true, null, gorilla.EventsParse);
	   
	gorilla.newsEventsCreated = true; //This is to run once.
};

gorilla.newsReleasesParse = function(req)
{
	var el = document.getElementById("news_releases");
	el.innerHTML = "";
    var rssjson = JSON.parse(req.responseText);
               
    for (var i=0; i<3; i++)
    {                             
    	var retObj = {
                mytitle : rssjson[i].title,
                mydescription : rssjson[i].description,
                mysubtitle : rssjson[i].subtitle,
                mylocation : rssjson[i].location,
                mydate : rssjson[i].formattedDateItem+" -"
        };
                   
        var div_date = document.createElement('div');
        	div_date.style.color='#5f5d5f';
	        div_date.style.fontSize='10px';
	        div_date.style.height='10px';
	        div_date.style.paddingTop='6px';
                               
        var div_title = document.createElement('div');
	        div_title.style.color='#009ED3';                 
	        div_title.style.fontSize='10px';
	        div_title.data = retObj;                 
	        div_title.onclick = function()
	        {                                                             
	            document.getElementById("specific_title_label").innerHTML = 'NEWS RELEASES';
	            document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase(); 
	            document.getElementById("specific_titleUpper_label").innerHTML = this.data.mytitle.toUpperCase();                                                                         
	           
	            document.getElementById("specific_subtitle_label").style.display = 'block';
	            document.getElementById("specific_subtitle_label").innerHTML = '';
	            if (this.data.mysubtitle != null)
	            {
		            document.getElementById("specific_subtitle_label").style.display = 'block';
		            document.getElementById("specific_subtitle_label").innerHTML = this.data.mysubtitle.toUpperCase(); 
	            }
                                                               
                var paragraph = this.data.mydescription.substring(0,3);                                
                var text = this.data.mydescription.substring(3);  
                
                
                if (this.data.mylocation == null)
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mydate+'</span>' + " " + text;
                }
                else
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mylocation+", "+this.data.mydate+'</span>' + " " + text;
                }
                            
                                                                            
                var el = document.getElementById("specificNewsAndEvents");
                el.style.fontSize = '11px';
                el.innerHTML = whole_text; //// needs fix                            
                gorilla.showPageById("SpecificNewsAndFeeds_page");
            };
       
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
       
       
        el.appendChild(div_date);
        el.appendChild(div_title);   
    }
};

gorilla.inThenewsParse = function(req)
{
	var el = document.getElementById("in_the_news");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
               
    for (var i=0; i<3; i++)
    {                             
   
    	var retObj = {
                mytitle : rssjson[i].title,
                mylink : rssjson[i].link           
        };
                   
        var div_date = document.createElement('div');
            div_date.style.color='#5f5d5f';
            div_date.style.fontSize='10px';  
            div_date.style.height='10px';
            div_date.style.paddingTop='6px';
                               
        var div_title = document.createElement('div');
            div_title.style.color='#009ED3';
            div_title.style.fontSize='10px';
            div_title.data = retObj;
            div_title.onclick= function(){                                                             
            	window.open(this.data.mylink);
            };
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
              
        el.appendChild(div_date);
        el.appendChild(div_title);   
    }
};

gorilla.EventsParse = function(req)
{
	var el = document.getElementById("events_cont");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	var total_num = rssjson.length;
	               
	if (total_num != 0)
	{                                             
	    //show container if there are events
	    document.getElementById("events_out_container").style.display="block";
	   
	    for (var i=0; i<total_num; i++)
	    {                                                
                                               
	        var retObj = {
	               mytitle : rssjson[i].title,
	               mydescription : rssjson[i].description            
	        };                
                                   
            var div_title = document.createElement('div');
                div_title.style.color='#009ED3';
                div_title.style.fontSize='10px';
                div_title.data = retObj;
                div_title.style.paddingTop='6px';
                div_title.onclick= function()
                {                                                             
	                
	                document.getElementById("specific_title_label").innerHTML = 'NEWS RELEASES';
	        		document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase();  
	        		document.getElementById("specific_titleUpper_label").innerHTML = ""          		        		
	        		
	        		document.getElementById("specific_subtitle_label").style.display = 'none';
	        		document.getElementById("specific_subtitle_label").innerHTML = ''; 
	        		
	        		document.getElementById("specific_subtitle_label").style.display = 'none';
	        		document.getElementById("specific_subtitle_label").innerHTML =""; 
        		              
	                
	                document.getElementById("specific_title_label").innerHTML = 'EVENTS';
	                document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase(); 
	               
	                document.getElementById("specific_titleUpper_label").innerHTML = this.data.mytitle.toUpperCase();                                                                                             
	                document.getElementById("specificNewsAndEvents").innerHTML = this.data.mydescription;                                  
	               
	                //show page
	                gorilla.showPageById("SpecificNewsAndFeeds_page");
                };
                   
                var div_description = document.createElement('div');
                    div_description.style.color='#5f5d5f';
                    div_description.style.fontSize='10px';
                    div_description.data = retObj;                  
                                
                div_title.innerHTML = retObj.mytitle;
                div_description.innerHTML = retObj.mydescription;
                       
                el.appendChild(div_title);
                el.appendChild(div_description);                                    
	    }//end for
               
	}//end if                                              
};
//---------------------|News and Events Page End|-------------------------//



//---------------------|AllNewsAndEvents Page|-------------------------//
gorilla.getAllNewsAndEventsPage = function(btn)
{		
	gorilla.switchLoadingIndicator(true);
	if (btn.id == "allreleases_btn")
	{		
		document.getElementById("title_label").innerHTML = "NEWS RELEASES";			
		veltijs.proxy.sendRequest(gorilla.services.showall_press_release_service, 'GET', true, null, gorilla.AllNewsReleasesParse);	
	}
	else if (btn.id == "allnews_btn")
	{
		document.getElementById("title_label").innerHTML = "IN THE NEWS";	
		veltijs.proxy.sendRequest(gorilla.services.showall_news_service, 'GET', true, null, gorilla.AllNewsItemsParse);	
	}
	else if (btn.id == "allevents_btn")
	{
		document.getElementById("title_label").innerHTML = "EVENTS";	
		veltijs.proxy.sendRequest(gorilla.services.showall_events_service, 'GET', true, null, gorilla.AllEventsItemsParse);	
	}
	
};

gorilla.AllNewsReleasesParse = function(req)
{
	var el = document.getElementById("allNewsAndEvents");
	el.innerHTML="";
	
	var rssjson = JSON.parse(req.responseText);
	for (var i=0; i<rssjson.length; i++)
	{		
	    
	    var retObj = {
			mytitle : rssjson[i].title,
	        mydescription : rssjson[i].description,
	        mysubtitle : rssjson[i].subtitle,
		    mylocation : rssjson[i].location,
		    mydate : rssjson[i].formattedDateItem+" -"
		};
	    	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='10px';	
	    	div_date.style.height='10px';
	    	div_date.style.paddingTop='6px';
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='10px';
        	div_title.data = retObj;
        	div_title.onclick= function()
        	{        		        		
        		document.getElementById("specific_title_label").innerHTML = 'NEWS RELEASES';
        		document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase();  
        		document.getElementById("specific_titleUpper_label").innerHTML = this.data.mytitle.toUpperCase();          		        		
        		
        		document.getElementById("specific_subtitle_label").style.display = 'block';
        		document.getElementById("specific_subtitle_label").innerHTML = ''; 
        		if (this.data.mysubtitle != null)
        		{
        			document.getElementById("specific_subtitle_label").style.display = 'block';
        			document.getElementById("specific_subtitle_label").innerHTML = this.data.mysubtitle.toUpperCase();  
        		}
        		        		
        		var paragraph = this.data.mydescription.substring(0,3);          		
                var text = this.data.mydescription.substring(3);
                
                
                if (this.data.mylocation == null)
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mydate+'</span>' + " " + text;
                }
                else
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mylocation+", "+this.data.mydate+'</span>' + " " + text;
                }
                
                
        		var el = document.getElementById("specificNewsAndEvents");
        		el.innerHTML = whole_text;        		
        		gorilla.showPageById('SpecificNewsAndFeeds_page');
        	};
        	
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
        
        el.appendChild(div_date);
        el.appendChild(div_title);	
	}	
	gorilla.switchLoadingIndicator(false);	
	gorilla.showPageById('AllNewsAndFeeds_page');
};

gorilla.AllNewsItemsParse = function(req)
{
	var el = document.getElementById("allNewsAndEvents");
	el.innerHTML="";
	
	var rssjson = JSON.parse(req.responseText);
	

	for (var i=0; i<rssjson.length; i++)
	{		                           
	    var retObj = {
    		mytitle : rssjson[i].title,
    		mylink : rssjson[i].link	        
		};
	    	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='10px';	 
	    	div_date.style.height='10px';
	    	div_date.style.paddingTop='6px';
	    	
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='10px';        	
        	div_title.data = retObj;
        	div_title.onclick= function()
        	{        		        		
        		window.open(this.data.mylink);
        	};
        	
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
        
        el.appendChild(div_date);
        el.appendChild(div_title);
        
        	
	}
	
	gorilla.switchLoadingIndicator(false);	
	gorilla.showPageById('AllNewsAndFeeds_page');
};
//---------------------|AllNewsAndEvents Page End|-------------------------//



gorilla.AllEventsItemsParse = function(req)
{
	var el = document.getElementById("allNewsAndEvents");
	el.innerHTML="";
	
	var rssjson = JSON.parse(req.responseText);
	console.log(rssjson);

	for (var i=0; i<rssjson.length; i++)
	{		                           
	    var retObj = {
    		mytitle : rssjson[i].title,
    		mydescription : rssjson[i].description	        
		};
	    	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='10px';	 
	    	div_date.style.height='10px';
	    	
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='10px';
        	div_date.style.paddingBottom='6px';
        	div_title.data = retObj;
        	div_title.onclick= function()
        	{        		        		
        		
        		document.getElementById("specific_title_label").innerHTML = 'NEWS RELEASES';
        		document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase();  
        		document.getElementById("specific_titleUpper_label").innerHTML = ""          		        		
        		
        		document.getElementById("specific_subtitle_label").style.display = 'none';
        		document.getElementById("specific_subtitle_label").innerHTML = ''; 
        		
        		document.getElementById("specific_subtitle_label").style.display = 'none';
        		document.getElementById("specific_subtitle_label").innerHTML =""; 
        		
        		
        		
        		document.getElementById("specific_title_label").innerHTML = 'EVENTS';
	            document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase(); 
	               
	            document.getElementById("specific_titleUpper_label").innerHTML = this.data.mytitle.toUpperCase();                                                                                             
	            document.getElementById("specificNewsAndEvents").innerHTML = this.data.mydescription;                                  
	               
	                //show page
	            gorilla.showPageById("SpecificNewsAndFeeds_page");
        	};
        	
        
        div_date.innerHTML = rssjson[i].description;
        div_title.innerHTML = retObj.mytitle;
        
        el.appendChild(div_title);
        el.appendChild(div_date);
        	
	}
	
	gorilla.switchLoadingIndicator(false);	
	gorilla.showPageById('AllNewsAndFeeds_page');
};


//---------------------|FAQ Page|-------------------------//
gorilla.faqCreated = false;

gorilla.getFaqPage = function()
{
	veltijs.proxy.sendRequest(gorilla.services.faq_service, 'GET', true, null, gorilla.faqPageParse);
		
	gorilla.faqCreated = true; //This is to run once.
};

gorilla.faqPageParse = function(req)
{
	var
	k = JSON.parse(req.responseText);
	
	var
	faqsContainer = document.getElementById('faqs-container');
	
	faqsContainer.innerHTML = '';
	
	for (var i=0; i<k.length; i++)
	{
		var description = k[i].description;
		
		k[i].description =
		(description.charAt(0) === "<" && description.charAt(1) === "p")
		?
		description
		:
		"<p>"+description+"</p>";
		
		faqsContainer.innerHTML +=
		'<div class="faqs-container-content">\
			<b>'+k[i].title+'</b><br/>'+k[i].description+'\
		</div>';
	}
};
//---------------------|FAQ Page End|-------------------------//



//---------------------|Praise Page|-------------------------//
gorilla.limitText = function (limitField, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} 
}

gorilla.praiseCreated = false;

gorilla.getPraisePage = function()
{	
	//every time enter page clear fields
	var nameField = document.getElementById('praise_name');
	var commentsField = document.getElementById('praise_comments');
	var validationPraiseField = document.getElementById('validation_praise');
	nameField.value = "";
	commentsField.value = "";
	validationPraiseField.innerHTML = "";
			
	veltijs.proxy.sendRequest(gorilla.services.media_service, 'GET', true, null, gorilla.mediaParse);
	veltijs.proxy.sendRequest(gorilla.services.consumers_service, 'GET', true, null, gorilla.consumersParse);
	
	gorilla.praiseCreated = true; //This is to run once.	
};

gorilla.mediaParse = function(req)
{
	var el = document.getElementById("media_releases");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<3; i++)
	{		
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
			mylegal : rssjson[i].legal,
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),	       
	        mydate : rssjson[i].formattedDate,
	        mylink : rssjson[i].link,		   		   
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='10px';
    		if (i==0)
    		{
    			div_desc.style.paddingTop='5px';
    		}
    		else
    		{
    			div_desc.style.paddingTop='20px';
    		}
	    
    	var flag_media = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_media = "true";	
    	}
    	
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='10px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='10px';
    	    
        var div_link = document.createElement('div');
        	div_link.style.color='#009ED3';
        	div_link.style.fontSize='10px';    
        	div_link.style.width='80px';
        	div_link.style.fontWeight = 'bold';
        	div_link.data = retObj;
        	div_link.onclick= function()
        	{        		        		
        		window.open(this.data.mylink);
        	};
        	
        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'Β®');       
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'Β®');
        div_date.innerHTML = retObj.mydate;
        div_link.innerHTML = "Learn More >>";
        
        el.appendChild(div_desc);
        if (flag_media == "true"){el.appendChild(div_legal);}        
        el.appendChild(div_title);	
        el.appendChild(div_date);	
        el.appendChild(div_link);	
        
	}
};

gorilla.consumersParse = function(req)
{
	var el = document.getElementById("consumers_releases");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<3; i++)
	{		
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),       
	        mydate : rssjson[i].formattedDate,
	        mylegal : rssjson[i].legal,
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='10px';    		
    		div_desc.style.paddingTop='10px';
    		
		var flag_cons = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_cons = "true";	
    	}
    		    		    		    		    		
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='10px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='10px';

        	        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'Β®');
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'Β®');
        div_date.innerHTML = retObj.mydate;      
        
        el.appendChild(div_desc);
        if (flag_cons == "true"){el.appendChild(div_legal);}   
        el.appendChild(div_title);	
        el.appendChild(div_date);	              
	}
};
//---------------------|Praise Page End|-------------------------//



//---------------------|AllPraise Page|-------------------------//
gorilla.getAllPraisePage = function(btn)
{		
	gorilla.switchLoadingIndicator(true);
			
	if (btn.id=="allmedia_btn")
	{				
		//hide submit form in case of media
		document.getElementById("allpraise_cont_new").style.display="none";
	
		document.getElementById("praise_label").innerHTML = "MEDIA";		
		veltijs.proxy.sendRequest(gorilla.services.media_service, 'GET', true, null, gorilla.AllMediaParse);	
	}
	else if (btn.id=="allconsumers_btn")
	{
		//show submit form in case of consumers
		document.getElementById("allpraise_cont_new").style.display="block";
		
		document.getElementById("praise_label").innerHTML = "CONSUMERS";	
		veltijs.proxy.sendRequest(gorilla.services.consumers_service, 'GET', true, null, gorilla.AllConsumersParse);
	}		
};

gorilla.AllMediaParse = function(req)
{
	var el = document.getElementById("allpraise_cont");
	el.innerHTML="";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<10; i++)
	{		
	    
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),	       
	        mydate : rssjson[i].formattedDate,
	        mylink : rssjson[i].link,	
	        mylegal : rssjson[i].legal,
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='10px';
    		if (i==0)
    		{
    			div_desc.style.paddingTop='5px';
    		}
    		else
    		{
    			div_desc.style.paddingTop='20px';
    		}
    		
    	var flag_all = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_all = "true";	
    	}	
    		    		    		
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='10px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='10px';
    	    
        var div_link = document.createElement('div');
        	div_link.style.color='#009ED3';
        	div_link.style.fontSize='10px';    
        	div_link.style.width='80px';
        	div_link.style.fontWeight = 'bold';
        	div_link.data = retObj;
        	div_link.onclick= function()
        	{        		        		
        		gorilla.openExternalLink(this.data.mylink);
        	};
        	
        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
        div_date.innerHTML = retObj.mydate;
        div_link.innerHTML = "Learn More >>";
        
        el.appendChild(div_desc);
        if (flag_all == "true"){el.appendChild(div_legal);}   
        el.appendChild(div_title);	
        el.appendChild(div_date);	
        el.appendChild(div_link);	
	}	
	
	gorilla.switchLoadingIndicator(false);
	gorilla.showPageById('AllPraise_page');
};

gorilla.AllConsumersParse = function(req)
{
	var el = document.getElementById("allpraise_cont");
	el.innerHTML="";	
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<25; i++)
	{		                           
		var retObj = {
				mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
		        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),       
		        mydate : rssjson[i].formattedDate,	        	   		   
			};				
			
		    var div_desc = document.createElement('div');
	    		div_desc.style.color='#000';
	    		div_desc.style.fontSize='10px'; 
	    		if (i==0)
	    		{
	    			div_desc.style.paddingTop='0px';
	    		}
	    		else
	    		{
	    			div_desc.style.paddingTop='12px';
	    		}
	    		
	    		    		    		    		    		
	        var div_title = document.createElement('div');
	        	div_title.style.color='#5f5d5f';        	
	        	div_title.style.fontSize='10px';
	        	div_title.style.fontWeight = 'bold';
	        	div_title.style.paddingTop='10px';
	        	
	    	var div_date = document.createElement('div');
	    	    div_date.style.color='#5f5d5f';
	    	    div_date.style.fontSize='10px';

	        	        	        	       	        				          
	        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');
	        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
	        div_date.innerHTML = retObj.mydate;      
	        
	        el.appendChild(div_desc);
	        el.appendChild(div_title);	
	        el.appendChild(div_date);	 
	}	
	
	gorilla.switchLoadingIndicator(false);
	gorilla.showPageById('AllPraise_page');
	
};
//---------------------|AllPraise Page End|-------------------------//



//---------------------|Innovating Page Start|-------------------------//

gorilla.setPage = function(pageToGo)
{
	var pagename = pageToGo.id.replace('btn','page');		
	gorilla.showPageById(pagename);
};

gorilla.setInnovatingpageMenu=function(num)
{
	var inno_menu = document.getElementById('inno_menu');
	for(var i=0;i<inno_menu.children.length;i++)
	{
		inno_menu.children[i].style.color="#AEAEAE";
	}
	inno_menu.children[num].style.color="#009ED3";
	var inno_menu_dots = document.getElementById('inno_menu_dots');
	for(var i=0;i<inno_menu_dots.children.length;i++)
	{
		inno_menu_dots.children[i].className="";
	}
	inno_menu_dots.children[num].className="active_inno_menu";
};

var label_id = '';

gorilla.openPDF = function(el)
{
    label_id = el.id; 
    //alert(label_id);
    var url ='';
    
    if (label_id == 'thin_pdf')
    {    	
    	veltijs.pushCustomEventAnalytics('thin_pdf');
    	url = "assets/PDFs/Thin Glass_ A New Design Reality_PDF.pdf";  
    }
    else if (label_id == 'product_pdf')
    {
    	veltijs.pushCustomEventAnalytics('product_pdf');
    	url = "assets/PDFs/Corning Gorilla Glass 2 Product Information Sheet.pdf"; 
    }
    else if (label_id == 'protective_pdf')
    {
    	veltijs.pushCustomEventAnalytics('protective_pdf');
    	url = "assets/PDFs/Protective Cover Glass for Portable Display Devices _PDF.pdf";    
    }
    else if (label_id == 'controlled_pdf')
    {
    	veltijs.pushCustomEventAnalytics('controlled_pdf');
    	url = "assets/PDFs/Controlled Edge Damage by Dynamic Impact_PDF.pdf";    
    }
    else if (label_id == 'easy_pdf')
    {
    	veltijs.pushCustomEventAnalytics('easy_pdf');
    	url = "assets/PDFs/Easy-to-Clean Surfaces for Mobile Devices_PDF.pdf";    
    }
    else if (label_id == 'specialty_pdf')
    {
    	veltijs.pushCustomEventAnalytics('specialty_pdf');
    	url = "assets/PDFs/Specialty GlassA New Design Element in Consumer Electronics _PDF.pdf";    
    }
    else if (label_id == 'cover_pdf')
    {
    	veltijs.pushCustomEventAnalytics('cover_pdf');
    	url = "assets/PDFs/Corning Gorilla Glass for Large Cover Applications.pdf";    
    }
    
    setTimeout(function(){
    	gorilla.openExternalLink(url);
	},100)
		
};
//---------------------|Innovating Page End|-------------------------//



// -----------------------|VIDEO PAGE|-----------------------------//
gorilla.setVideopageMenu=function(num)
{
	var inno_menu = document.getElementById('video_menu');
	for(var i=0;i<inno_menu.children.length;i++)
	{
		inno_menu.children[i].style.color="#AEAEAE";
	}
	inno_menu.children[num].style.color="#009ED3";
	var inno_menu_dots = document.getElementById('video_menu_dots');
	for(var i=0;i<inno_menu_dots.children.length;i++)
	{
		inno_menu_dots.children[i].className="";
	}
	inno_menu_dots.children[num].className="active_inno_menu";
};

gorilla.setVideoPreviewPage = function(url)
{

	if (url == "http://download.velti.gr.edgesuite.net/gor/TV_30_bdcst_mix_.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('victor1');    	
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Kitchen30_bdcast_mix_1080p_.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('victor2');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Office_30_bdcst_mix_.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('victor3');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/How_Corning_tests_Glass.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('demos1');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Gorilla_Tough.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('saying1');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Gorilla_2_and_Microsoft.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('saying2');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Corning_ADayMadeofGlass.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('brought1');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Corning_modern_design_video.mp4"){
		//analytics
    	veltijs.pushCustomEventAnalytics('brought2');
	}
	else if (url == "http://download.velti.gr.edgesuite.net/gor/Changing the Way We Think about Glass.mp4"){
		//analytics		
    	veltijs.pushCustomEventAnalytics('brought3');
	}	
	setTimeout(function(){
		
		if (window.navigator.userAgent.toLowerCase().search('android') > -1 || window.navigator.userAgent.toLowerCase().search('blackberry') > -1)
		{
			window.location.href = url;
		}
		else
		{
			var vidCont = document.getElementById('previewVideoCont');
			
			vidCont.src = url;
			vidCont.load();
			vidCont.play();
		}
	},100);
	
	
}

gorilla.createVideoCarousels = function()
{
	 var videoToCreate ={
			victor:
			{
				divName:"carouselVideo1",
	            labelName:"carouselThumb1",
	            container:"videocont1",
	            containerl:"videocont1l",
	            pics:['assets/videosPage/channel_surfing.jpg',
	                  'assets/videosPage/cooking_up.jpg',
	                  'assets/videosPage/king_of_the_office.jpg'],
	            labels:['assets/videosPage/channel_surfing_label.png',
	                    'assets/videosPage/cooking_up_label.png',
	                    'assets/videosPage/king_of_the_office_label.png'],          
	            actions:[
	                    function()
	                    {      	        	                    	
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor1');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/TV_30_bdcst_mix_.mp4");
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Channel Surfing", "Videos - Main"];	                    	
	                    },
	                    function()                    
	                    { 	        
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor2');
	                    		                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Kitchen30_bdcast_mix_1080p_.mp4");
	                    		                    	
	                    	// var args = ["trackEvent", "GorillaVideos", "Video Views", "Cooking Up Tomorrow's Kitchen", "Videos - Main"];
	                    },
	                    function()
	                    {	                    	 
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor3');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Office_30_bdcst_mix_.mp4");
	                    		                    		                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "King of the Office", "Videos - Main"];
	                    }
	                ]
			},
			demos:
			{
				divName:"carouselVideo2",
				labelName:"carouselThumb2",
				container:"videocont2",
				containerl:"videocont2l",
				pics:['assets/videosPage/how_corning_tests.jpg'],
				labels:['assets/videosPage/how_corning_tests_label.png'],
				actions :[
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('demos1');
	                	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/How_Corning_tests_Glass.mp4");
	                		                	                    	
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "How Corning Tests Gorilla Glass", "Videos - Main"];
	                }
	            ]
			},
			saying:
	        {
				divName:"carouselVideo3",
				labelName:"carouselThumb3",
	            container:"videocont3",
	            containerl:"videocont3l",
	            pics:['assets/videosPage/gorilla_tough.jpg',
	                  'assets/videosPage/microsoft.jpg'],
	            labels:['assets/videosPage/gorilla_tough_label.png',
	                    'assets/videosPage/microsoft_label.png'],
	            actions:[
	                    function()
	                    {
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('saying1');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Gorilla_Tough.mp4");	                    		                    
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Gorilla Tough", "Videos - Main"];	    	                
	                    },
			            function()
			            {
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('saying2');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Gorilla_2_and_Microsoft.mp4");	                    		                    	
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos - Main"];
			            }
	            ]
	        },
	        brought:
	        {
	          divName:"carouselVideo4",
	          labelName:"carouselThumb4",
	          container:"videocont4",
	          containerl:"videocont4l",
	          pics:['assets/videosPage/a_day_made_of.jpg',
	                'assets/videosPage/design.jpg',
	                'assets/videosPage/the_way_we_think.jpg'],
	          labels:['assets/videosPage/a_day_made_of_label.png',
	                  'assets/videosPage/design_label.png',
	                  'assets/videosPage/the_way_we_think_label.png'],
	          actions:[
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought1');
                    	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Corning_ADayMadeofGlass.mp4");    	                	                	
                    	
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "A Day Made of Glass", "Videos - Main"];			            
	                },
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought2');
                    	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Corning_modern_design_video.mp4");
    	                
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Corning Gorilla Glass for Seamless Elegant Design", "Videos - Main"];			            
	                },
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought3');
                    	
	                	gorilla.setVideoPreviewPage("http://meds.mblgrt.com/ch/36585/Changing_the_Way_We_Think");    	             	                
                    
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Changing the Way We Think about Glass", "Videos - Main"];			            
	                }
	            ]
	        }
		}
		
	 	
	 	for(var i=0;i<Object.keys(videoToCreate).length;i++)
	 	{
	 		var attr = Object.keys(videoToCreate)[i];
	 		if(videoToCreate[attr].pics.length==1)
	 			gorilla.coverflowVideos(videoToCreate[attr].divName,videoToCreate[attr].labelName,videoToCreate[attr].container,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);   
		 	else
	 		gorilla.vCarousel(videoToCreate[attr].container,videoToCreate[attr].divName,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);
	 		
	 		gorilla.coverflowVideos(videoToCreate[attr].divName,videoToCreate[attr].labelName,videoToCreate[attr].containerl,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);   
	 	} 
	}


//---------------------|Fun With Gorilla Page|-------------------------//
gorilla.playMP3 = function(el)
{
	var button_id = el.id;
	var audio = document.getElementById("gorilla_ringtones");
	var isOldPhone = (window.navigator.userAgent.toLowerCase().search('blackberry') > -1 || window.navigator.userAgent.toLowerCase().search('iphone os 4_') > -1 || (window.navigator.userAgent.toLowerCase().search('version/4.') > -1));
    
    if (button_id == "hootsmp3_pr")
    {  
    	//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_hoots');
        
		if (isOldPhone)
		{
			window.location.href = 'assets/funWithGorillaPage/audio/hoots.mp3';
		}
		else
		{
			audio.src='assets/funWithGorillaPage/audio/hoots.mp3';
		}
	}
	else if (button_id == "victormp3_pr")
	{ 
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_victor');

		if (isOldPhone)
		{
			window.location.href = 'assets/funWithGorillaPage/audio/victor.mp3';
		}
		else
		{
			audio.src='assets/funWithGorillaPage/audio/victor.mp3';
		}
	}
	else if (button_id == "clubmp3_pr")
	{ 
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_club');
		
		if (isOldPhone)
		{
			window.location.href = 'assets/funWithGorillaPage/audio/club.mp3';
		}
		else
		{
			audio.src='assets/funWithGorillaPage/audio/club.mp3';
		}
	}
	else if (button_id == "congomp3_pr")
	{
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_congo');
		
		if (isOldPhone)
		{
			window.location.href = 'assets/funWithGorillaPage/audio/congo.mp3';
		}
		else
		{
			audio.src='assets/funWithGorillaPage/audio/congo.mp3';
		}
	}
	
	audio.load();
    setTimeout(function(){
		audio.play();
	}, 500)   
};
 
gorilla.ringTonechoise="";

gorilla.sendRingTone= function(btn)
{
       gorilla.ringTonechoise = btn.id;       
       gorilla.showPageById('GorillaEmail_page');
};



//---------------------------------|Email Page|------------------------------------------
gorilla.clearEmaillabel = function()
{
	//every time enter page clear Email label
	var validation_msg = document.getElementById('validation_msg');
	validation_msg.innerHTML="";   
	
	var emailField = document.getElementById('gorilla_email_ringtone_val');
	emailField.value="";
};

gorilla.sendFileViaEmail = function(w)
{    
	var validation_msg = document.getElementById('validation_msg');
    var email = document.getElementById('gorilla_email_ringtone_val').value;
       
    if (email == '')
    {
    	//fill Email label
    	validation_msg.innerHTML = "Please enter an email address.";
    }
    else
    {	 
	    //check email if it's correct
		var flagEmail = gorilla.isValidEmail(email);
		
		//if it's not correct
		if (flagEmail == false)
		{
			validation_msg.innerHTML = "Please enter a valid email address.";
		}
		else
		{
		  var actualFile = '';    
		  if(gorilla.ringTonechoise != '')
		  {
			  switch (gorilla.ringTonechoise)
			  {
				  case 'hootsmp3' :
				  actualFile = 'HootsGrunts.mp3';
				  break;
				  
				  case 'hootsm4r':
				  actualFile = 'HootsGrunts.m4r';
				  break;
				  
				  case 'victorm4r':
				  actualFile = 'VictorsGmix.m4r';
				  break;
				  
				  case 'victormp3':
				  actualFile = 'VictorsGmix.mp3';
				  break;
				  
				  case 'clubmp3':
				  actualFile = 'ClubGorilla.mp3';
				  break;
				  
				  case 'clubm4r':
				  actualFile = 'ClubGorilla.m4r';
				  break;
                                        
                  case 'congomp3':
                  actualFile = 'CallsfortheCongo.mp3';
                  break;
                  
                  case 'congom4r':
                  actualFile = 'CallsfortheCongo.m4r';
                  break;                                                					  
			  }				  
			  //subject
			  var subject = "Ringtone from a Friend";
			  
			  //body
			  var bodyTemplate = "A friend would like to share this ringtone with you from the Corning Gorilla Glass website";
			  
			  var data =
			  {
				  'formMap':gorilla.services.sendfile_viaemail_service,
				  'instanceId':'AEAAF8AB-490A-BD17-8562-DD8615B3B949',
				  'system_send_name':'CorningGorillaGlass',
				  'system_send_email':'GorillaGlass@vendor.com',
				  'showFriendsName':'false',
				  'showSendersEmail':'false',
				  'showSendersName':'false',
				  'friendEmail':email,
				  'optOutURL':'',
				  'attachementFileName':actualFile,
				  'bodyTemplate':bodyTemplate,
				  'subject':subject
			  };
			  
			  veltijs.proxy.sendRequest(gorilla.services.sendfile_viaemail_host, 'GET', 'true', data, gorilla.responseSendviaEmail);
		  }  
		}//end correct email
  
    }//end if
};



//function to check valid email address
gorilla.isValidEmail = function(strEmail)
{    
	var validRegExp = new RegExp('[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})','i');
	
	var contains_space = strEmail.indexOf(' ');
	//alert(contains_space);
	if (contains_space >= 0){		
		return false;
	}
	
	//search email text for regular exp matches
	if (validRegExp.test(strEmail)){
		return true;
	}
	else{
		return false;
	}	
};

gorilla.responseSendviaEmail = function(req)
{
	var validation_msg = document.getElementById('validation_msg');
	var email = document.getElementById('gorilla_email_ringtone_val');
	
	if (req.responseText == ""){
        //fill Email label
		validation_msg.innerHTML = "Thank you. Your email has been sent.";
		
		//download analytics
		veltijs.pushCustomEventAnalytics(gorilla.ringTonechoise);
		
		//clear textbox
		email.value="";
    }
	else{
		//fill Email label
		validation_msg.innerHTML = "You have exceeded the limit of 5 emails/day. Please try again later.";
		
        //clear textbox
		email.value="";
	}
};
//---------------------|Email Page|-------------------------//



//---------------------- Praise Submit Button ----------------------------
gorilla.praise_commit = function()
{    	
    var pname = document.getElementById('praise_name').value;
    var pcomments = document.getElementById('praise_comments').value;    
    
    if ((pname != "") && (pcomments != ""))
    {         	
    	//loading indicator
    	gorilla.switchLoadingIndicator(true);
    	
	    var data = {'name':pname, 'comment':pcomments};		    
	    veltijs.proxy.sendRequest(gorilla.services.submit_service, 'GET', true, data, gorilla.commit_praise_res);	
	    
	    //Every time button pressed clear fields
		var nameFieldCons = document.getElementById('praise_name');
		var commentsFieldCons = document.getElementById('praise_comments');
		var validationField = document.getElementById('validation_praise');
		nameFieldCons.value = "";
		commentsFieldCons.value = "";
		validationField.innerHTML = "";
    }
    else
    {
    	document.getElementById('validation_praise').innerHTML = "Please insert your name and add your comment.";
    }
};



//---------------------- Praise consumers Submit Button ----------------------------
gorilla.praise_commit_consumers = function()
{
	var pnameCons = document.getElementById('praise_name_consumers').value;
    var pcommentsCons = document.getElementById('praise_comments_consumers').value;
    
    if ((pnameCons != "") && (pcommentsCons != ""))
    {       
    	//loading indicator
    	gorilla.switchLoadingIndicator(true);
    	
	  	var data ={'name':pnameCons, 'comment':pcommentsCons};	
	    veltijs.proxy.sendRequest(gorilla.services.submit_service, 'GET', true, data, gorilla.commit_praise_res);	      		         
	    
	    //every time button pressed clear fields
		var nameFieldCons = document.getElementById('praise_name_consumers');
		var commentsFieldCons = document.getElementById('praise_comments_consumers');
		var validationAllField = document.getElementById('validation_allpraise');
		nameFieldCons.value = "";
		commentsFieldCons.value = "";
		validationAllField.innerHTML = "";  
    }
    else
    {
    	document.getElementById('validation_allpraise').innerHTML="Please insert your name and add your comment.";
    }
};

gorilla.commit_praise_res = function(req)
{
    var res = JSON.parse(req.responseText);   
    if(res.submited)
    {
    	gorilla.switchLoadingIndicator(false);
    	
        //show thank you page
    	gorilla.showPageById("PraiseThankYou_page");    	
    }
    else
    {    	
        //error message
        res.message;
    }
};



//---------------------Popup Blocker----------------
gorilla.closePopupBlocker = function()
{
	var popup = document.getElementById("popupBlocker");
	popup.style.display = "none";
}

gorilla.showPopupBlocker = function()
{
	
	var popup = document.getElementById("popupBlocker");
	popup.style.top = window.pageYOffset;
	popup.style.height = '120%';
	popup.style.display = "block";
}

gorilla.openExternalLink = function(mylink)
{
	
	var newwin = window.open(mylink,'_blank');
    if(!newwin){
		gorilla.showPopupBlocker();
    }
}
//---------------------Popup Blocker----------------



//---------------------|Sharing Functionality|-------------------------//
gorilla.socialSharing = function(btn)
{
	var	button_id = btn.id;
	
	var	share_ext = button_id.split('_')[1];
	
	switch(share_ext)
	{
	case 'sms':
		gorilla.smsPrevPage = location.hash.replace('#', '');
		gorilla.smsButtonId = button_id;
		gorilla.GenericShare(button_id, share_ext);
		gorilla.showPageById('GorillaSMS_page');
		break;
		
	case 'fb':
	case 'tw':
	case 'email':
		gorilla.GenericShare(button_id, share_ext);
		break;
	}
};


//--------------------------- FACEBOOK ----------------------------

var innovate_current_share  = '',
	videos_current_share    = '',
	videos_selected_item_1  =  0,
	videos_selected_item_2  =  0,
	videos_selected_item_3  =  0,
	videos_selected_item_4  =  0,
	wallpaper_selected_item = -1,
	sms_phone = '',
	sms_name  = '';

gorilla.GenericShare = function(idButton, ext)
{
	var
	page  = '', //FACEBOOK
	text  = '', //TWITTER
	subj  = '', //EMAIL
	body  = ''; //EMAIL
	msgId = ''; //SMS
	
	switch(idButton)
	{
	case 'home_'+ext:
		page  = 'page1';
		text  = 'Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out @ www.corninggorillaglass.com';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out http://www.corninggorillaglass.com';
		msgId = '1';
		break;
		
	case 'products_'+ext:
		page  = 'page2';
		text  = 'Look for Corning® Gorilla® Glass on 425 product models. Is it on yours? Check it out at http://bit.ly/oJ4pxI';
		subj  = 'Tough Yet Beautiful';
		body  = 'Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out @ http://www.corninggorillaglass.com/#products-with-gorilla';
		msgId = '2';
		break;
		
	case 'productsfull_'+ext:
		page  = 'page3';
		text  = 'Look for Corning® Gorilla® Glass on 30 major brands. Is it on yours? Check it out @ http://bit.ly/qHKBVK';
		subj  = 'Tough Yet Beautiful';
		body  = 'Look for Corning® Gorilla® Glass on 425 product models. Is it on yours? Check it out @ http://www.corninggorillaglass.com/products-with-gorilla/full-products-list';
		msgId = '3';
		break;
		
	case 'innovating_'+ext:
		switch(innovate_current_share)
		{
		case 'overview':
			page  = 'page21';
			text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/mS71FT';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/innovating-with-gorilla';
			msgId = '21';
			veltijs.pushCustomEventAnalytics('over_'+ext);
			break;
			
		case 'characteristics':
			page  = 'page22';
			text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/qzZPVO';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/characteristics';
			msgId = '22';
			veltijs.pushCustomEventAnalytics('chara_'+ext);
			break;
			
		case 'customization':
			page  = 'page23';
			text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/nXeJZG';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/customization';
			msgId = '23';
			veltijs.pushCustomEventAnalytics('custo_'+ext);
			break;
			
		case 'applications':
			page  = 'page24';
			text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/rnebhj';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/applications';
			msgId = '24';
			veltijs.pushCustomEventAnalytics('appli_'+ext);
			break;
			
		case 'literature':
			page  = 'page25';
			text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/q1TIBV';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/literature';
			msgId = '25';
			veltijs.pushCustomEventAnalytics('lite_'+ext);
			break;
			
		default:
			page = 'page20';
			text = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/oIlUvz';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Corning® Gorilla® Glass\'s is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/innovating-with-gorilla';
			msgId = '21';
			veltijs.pushCustomEventAnalytics('over_'+ext);
			break;
		}
		break;
		
	case 'news_'+ext:
		page  = 'page18';
		text  = 'Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/#news-events';
		msgId = '19';
		break;
		
	case 'specificnews_'+ext:
		page  = 'page19';
		text  = 'Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events'; 
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/#news-events';
		msgId = '20';
		break;
		
	case 'videos_'+ext:
		switch(videos_current_share)
		{
		case 'victor':
			switch(videos_selected_item_1)
			{
			case 0:
				page  = 'page33';
				text  = 'Watch Corning® Gorilla® Glass Channel Surfing Video @ http://youtu.be/5Qxknat5o_Q';
				subj  = 'Corning® Gorilla® Glass CHANNEL SURFING';
				body  = 'Check out the \'Channel Surfing\' video Corning® Gorilla® Glass Video @ http://youtu.be/5Qxknat5o_Q';
				msgId = '32';
				veltijs.pushCustomEventAnalytics('victor1_'+ext);
				break;
				
			case 1:
				page  = 'page34';
				text  = 'Watch Corning® Gorilla® Glass COOKING UP TOMORROWS KITCHEN Video @ http://youtu.be/ZUQEFL45Iko';
				subj  = 'Corning® Gorilla® Glass TOMORROWS KITCHEN';
				body  = 'Check out the \'Cooking up tomorrow\'s kitchen\' video by CorningΒ® GorillaΒ® Glass Video @ http://youtu.be/ZUQEFL45Iko';
				msgId = '33';
				veltijs.pushCustomEventAnalytics('victor2_'+ext);
				break;
				
			case 2:
				page  = 'page35';
				text  = 'Watch Corning® Gorilla® Glass KING OF THE OFFICE Video @ http://youtu.be/X0MhSIFgW9U';
				subj  = 'Corning® Gorilla® Glass KING OF THE OFFICE';
				body  = 'Check out the \'King of the Office\' video by Corning® Gorilla® Glass Video @ http://youtu.be/X0MhSIFgW9U';
				msgId = '34';
				veltijs.pushCustomEventAnalytics('victor3_'+ext);
				break;
			}
			break;
			
		case 'demos':
			switch(videos_selected_item_2)
			{
			case 0:
				page  = 'page51';
				text  = 'Watch Corning® Gorilla® Glass HOW CORNING TESTS Video @ http://www.corninggorillaglass.com/videos';
				subj  = 'Corning® Gorilla® Glass HOW CORNING TESTS';
				body  = 'Check out this CorningΒ® GorillaΒ® Glass Video @ http://www.corninggorillaglass.com/videos';
				msgId = '102';
				veltijs.pushCustomEventAnalytics('demos1_'+ext);
				break;
			}
			break;
			
		case 'saying':
			switch(videos_selected_item_3)
			{
			case 0:
				page  = 'page38';
				text  = 'Watch Corning® Gorilla® Glass GORILLA TOUGH Video @ http://www.corninggorillaglass.com/videos';
				subj  = 'Corning® Gorilla® Glass GORILLA TOUGH';
				body  = 'Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				msgId = '37';
				veltijs.pushCustomEventAnalytics('saying1_'+ext);
				break;
				
			case 1:
				page  = 'page50';
				text  = 'Watch Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				subj  = 'Corning® Gorilla® Glass GORILLA 2 AND MICROSOFT';
				body  = 'Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				msgId = '102';
				veltijs.pushCustomEventAnalytics('saying2_'+ext);
				break;
			}
			break;
			
		case 'brought':
			switch(videos_selected_item_4)
			{
			case 0:
				page  = 'page39';
				text  = 'Watch Corning® Gorilla® Glass A DAY MADE OF GLASS Video @ http://youtu.be/6Cf7IL_eZ38';
				subj  = 'Corning® Gorilla® Glass A DAY MADE OF GLASS';
				body  = 'Check out the video \'A Day Made of Glass\' by CorningΒ® GorillaΒ® Glass @ http://youtu.be/6Cf7IL_eZ38';
				msgId = '38';
				veltijs.pushCustomEventAnalytics('brought1_'+ext);
				break;
				
			case 1:
				page  = 'page49';
				text  = 'Watch Corning® Gorilla® Glass MODERN DESIGN Video @ http://www.corninggorillaglass.com/videos';
				subj  = 'Corning® Gorilla® Glass MODERN DESIGN';
				body  = 'Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				msgId = '102';
				veltijs.pushCustomEventAnalytics('brought2_'+ext);
				break;
				
			case 2:
				page  = 'page52';
				text  = 'Watch Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				subj  = 'Corning® Gorilla® Glass CHANGING THE WAY WE THINK ABOUT GLASS';
				body  = 'Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos';
				msgId = '141';
				veltijs.pushCustomEventAnalytics('brought3_'+ext);
				break;
			}
			break;
			
		default:
			page = 'page32';
			text = 'Watch the latest videos about Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/videos';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Watch the latest videos about Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/videos';
			msgId = '31';
			veltijs.pushCustomEventAnalytics('videos_'+ext);
			break;
		}
		break;
		
	case 'fun_'+ext:
		switch(wallpaper_selected_item)
		{
		case 0:
			page  = 'page31';
			text  = 'Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/nRgqg1';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/cdn/29645/Corning_GorillaGlass_Wall';
			msgId = '30';
			veltijs.pushCustomEventAnalytics('coverflow0pic1_'+ext);
			break;
			
		case 1:
			page  = 'page29';
			text  = 'Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/nLGTaS';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/cdn/29621/Corning_GorillaGlass_Wall';
			msgId = '29';
			veltijs.pushCustomEventAnalytics('coverflow0pic2_'+ext);
			break;
			
		case 2:
			page  = 'page30';
			text  = 'Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/pw15cT';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/cdn/29629/Corning_GorillaGlass_Wall';
			msgId = '27';
			veltijs.pushCustomEventAnalytics('coverflow0pic3_'+ext);
			break;
			
		case 3:
			page  = 'page28';
			text  = 'Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/o43IGW';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/cdn/29637/Corning_GorillaGlass_Wall';
			msgId = '28';
			veltijs.pushCustomEventAnalytics('coverflow0pic4_'+ext);
			break;
			
		case 4:
			page  = 'page48';
			text  = 'Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://meds.mblgrt.com/ch/36463/Fossy_Wallpapers';
			subj  = 'Corning® Gorilla® Glass';
			body  = 'Download your own awesome Fossey wallpaper @ http://meds.mblgrt.com/ch/36463/Fossy_Wallpapers';
			msgId = '101';
			veltijs.pushCustomEventAnalytics('coverflow0pic5_'+ext);
			break;
			
		default:
			page  = 'page26';
			text  = 'Download your own awesome Victor the Gorilla wallpaper or video @ http://www.corninggorillaglass.com/fun-with-gorilla';
			subj  = 'Corning® Gorilla® Glass - Wallpapers';
			body  = 'Download your own awesome Victor the Gorilla wallpaper or ringtone @ http://www.corninggorillaglass.com/fun-with-gorilla';
			msgId = '26';
			veltijs.pushCustomEventAnalytics('fun_'+ext);
			break;
		}
		break;
		
	case 'faqs_'+ext:
		page  = 'page41';
		text  = 'Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/faqs';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Corning® Gorilla® Glass is changing the way the world thinks about glass. Have questions? Check us out at http://www.corninggorillaglass.com/#faqs';
		msgId = '39';
		break;
		
	case 'praise_'+ext:
		page  = 'page44';
		text  = 'Want to show your praise for Corning® Gorilla® Glass? Check us out at http://www.corninggorillaglass.com/praise-for-gorilla';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Want to show your praise for Corning® Gorilla® Glass? Send us your feedback at http://www.corninggorillaglass.com/praise-for-gorilla';
		msgId = '81';
		break;
		
	case 'allpraise_'+ext:
		page  = 'page44';
		text  = 'Want to show your praise for Corning® Gorilla® Glass? Check us out at http://www.corninggorillaglass.com/praise-for-gorilla';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Want to show your praise for Corning® Gorilla® Glass? Send us your feedback at http://www.corninggorillaglass.com/praise-for-gorilla';
		msgId = '81';
		break;
		
	case 'acer_'+ext:
		page  = 'page4';
		text  = 'Check out Acer\'s products featuring Corning® Gorilla® Glass @ http://bit.ly/kIxpI7';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Acer\'s products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/acer';
		msgId = '4';
		break;
		
	case 'dell_'+ext:
		page  = 'page6';
		text  = 'Check out Dell products featuring Corning® Gorilla® Glass @ http://bit.ly/qdbLQD';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Dell products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/dell';
		msgId = '6';
		break;
		
	case 'htc_'+ext:
		page  = 'page7';
		text  = 'Check out HTC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/htc';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out HTC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/htc';
		msgId = '7';
		break;
		
	case 'kyo_'+ext:
		page  = 'page8';
		text  = 'Check out Kyocera products featuring Corning® Gorilla® Glass @ http://bit.ly/nOL9U2';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Kyocera products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/kyocera';
		msgId = '8';
		break;
		
	case 'asus_'+ext:
		page  = 'page5';
		text  = 'Check out Asus® products featuring Corning® Gorilla® Glass @ http://bit.ly/r3XOwk';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Asus® products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/asus-computer';
		msgId = '5';
		break;
		
	case 'lg_'+ext:
		page  = 'page10';
		text  = 'Check out LG products featuring Corning® Gorilla® Glass @ http://bit.ly/qvzyLL';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out LG products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/lg';
		msgId = '10';
		break;
		
	case 'moto_'+ext:
		page  = 'page11';
		text  = 'Check out Motorola products featuring Corning® Gorilla® Glass @ http://bit.ly/o3lo1s';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Motorola products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/motorola';
		msgId = '11';
		break;
		
	case 'moti_'+ext:
		page  = 'page12';
		text  = 'Check out Motion Computing products featuring Corning® Gorilla® Glass @ http://bit.ly/riTX1E';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Motion Computing products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/motion-computing';
		msgId = '12';
		break;
		
	case 'nec_'+ext:
		page  = 'page13';
		text  = 'Check out NEC products featuring Corning® Gorilla® Glass @ http://bit.ly/puEQeR';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out NEC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/nec';
		msgId = '13';
		break;
		
	case 'nokia_'+ext:
		page  = 'page14';
		text  = 'Check out Nokia products featuring Corning® Gorilla® Glass at http://bit.ly/ouBGWW';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Nokia products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/nokia';
		msgId = '14';
		break;
		
	case 'sam_'+ext:
		page  = 'page15';
		text  = 'Check out Samsung products featuring Corning® Gorilla® Glass @ http://bit.ly/qEoNkM';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Samsung products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/samsung';
		msgId = '15';
		break;
		
	case 'sk_'+ext:
		page  = 'page16';
		text  = 'Check out SK Telesys products featuring Corning® Gorilla® Glass @ http://bit.ly/qMcBtP';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out SK Telesys products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/sk-telesys';
		msgId = '16';
		break;
		
	case 'sony_'+ext:
		page  = 'page17';
		text  = 'Check out Sony products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/SonyBRAVIA/for_bravia.html';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Sony products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/SonyBRAVIA/for_bravia.html';
		msgId = '17';
		break;
		
	case 'lenovo_'+ext:
		page  = 'page9';
		text  = 'Check out Lenovo products featuring Corning® Gorilla® Glass @ http://bit.ly/px9QUK';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Lenovo products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/lenovo';
		msgId = '9';
		break;
		
	case 'hyundai_'+ext:
		page  = 'page40';
		text  = 'Check out Hyundai products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/products-with-gorilla/hyundai';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Hyundai products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/hyundai';
		msgId = '18';
		break;
		
	case 'sur40_'+ext:
		page  = 'page46';
		text  = 'Check out Samsung products featuring Corning® Gorilla® Glass @ http://bit.ly/Hx4e9j';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Samsung products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/samsungsur40';
		msgId = '62';
		break;
		
	case 'hp_'+ext:
		page  = 'page45';
		text  = 'Check out HP products featuring Corning® Gorilla® Glass @ http://bit.ly/I5bn2e';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out HP products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/hp';
		msgId = '61';
		break;
		
	case 'sonim_'+ext:
		page  = 'page47';
		text  = 'Check out Sonim products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/products-with-gorilla/sonim';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Sonim products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/sonim';
		msgId = '82';
		break;
		
	case 'lumigon_'+ext:
		page  = 'page53';
		text  = 'Check out Lumigon products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/product/lumigon';
		subj  = 'Corning® Gorilla® Glass';
		body  = 'Check out Lumigon products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/product/lumigon';
		msgId = '142';
		break;
	}
	
	switch(ext)
	{
	case 'fb':
		if (page === '')
		{
			alert('Facebook button id -> "'+idButton+'" not found.');
			return;
		}
		
		var url = gorilla.services.facebook_url+page+((window.location.href.search('corninggorillaglass') > -1) ? '&redirect=true' : '&noredirect=true');
		
		window.setTimeout(function()
		{
			gorilla.openExternalLink('http://www.facebook.com/sharer.php?u='+escape(url));
		},
		500);
		break;
		
	case 'tw':
		if (text === '')
		{
			alert('Twitter button id -> "'+idButton+'" not found.');
			return;
		}
		window.setTimeout(function()
		{
			gorilla.openExternalLink('http://twitter.com/share?url=&text='+encodeURI(text));
		},
		30);
		break;
		
	case 'email':
		if (subj === '' && body === '')
		{
			alert('Email button id -> "'+idButton+'" not found.');
			return;
		}
		window.setTimeout(function()
		{		
			window.location.href = 'mailto:?subject='+subj+'&body='+body;
		},
		500);
		break;
		
	case 'sms':
		if (msgId === '')
		{
			alert('SMS button id -> "'+idButton+'" not found.');
			return;
		}
		var jsonObj =
		{
			'clientId' : 'corning_gorilla',
			'msgId'    : msgId,
			'msisdns'  : [sms_phone],
			'params'   : { 'name' : sms_name }
		};
		
		var headers =
		{
			'Content-Type' : veltijs.proxy.TYPE_JSON,
			'Accept'       : 'application/json'
		};
		
		veltijs.proxy.sendRequest(gorilla.services.sms_service, 'POST', true, jsonObj, gorilla.responseShareViaSMS, headers);
		break;
	}
}


//----------------------- Share Via Sms ----------------------------------
gorilla.clearSmslabel = function()
{
	//clear textfields and labels
	document.getElementById('sms_name').value = "";
    document.getElementById('sms_phone').value = "";
	
    document.getElementById('validation_sms').innerHTML = "";
}

gorilla.shareViaSMS = function()
{
	var sms_validation_msg = document.getElementById('validation_sms');
	sms_validation_msg.innerHTML = '';
	
	sms_name = document.getElementById('sms_name').value;
	var sms_pre_phone = document.getElementById('sms_phone').value;
	var sms_count = sms_pre_phone.length;
	
	if (sms_pre_phone == '' || sms_count < 10 || sms_count > 10)
	{
		sms_validation_msg.innerHTML = 'Please insert a 10-digit phone number.';
	}
	else
	{
		sms_phone = '+1' + sms_pre_phone;
		gorilla.GenericShare(gorilla.smsButtonId, 'sms');
	}
};

gorilla.cancelSms = function()
{
	gorilla.showPageById(gorilla.smsPrevPage);
}

gorilla.responseShareViaSMS = function(req)
{       
	var response = req.responseText;
	//alert(response);
	
	var sms_validation_msg = document.getElementById('validation_sms');
	
	if (response.search("ORDERID") != -1) 
	{
		sms_validation_msg.innerHTML="Thank you. This message has been successfully sent to your mobile.";
	}
	else
	{
		sms_validation_msg.innerHTML="I'm sorry, please try again.";
	}
};

gorilla.addScroll = function (id){
	//setTimeout(function(){
	console.log("running scroller");
	var my_mywrapper = document.getElementById(id);
	var scroller = my_mywrapper.children[0];//document.getElementById("scroller");
	var thelist = scroller.children[0];//document.getElementById("thelist");
	var itemslength = thelist.children.length;
	//alert(itemslength);
	scroller.style.width = (itemslength *70) +'px';
	var myScroll = new iScroll(id,{hScrollbar:false });
	gorilla.currentScroller = myScroll;
	//},100);
};

gorilla.addScroll2 = function(id)
{	
	console.log("running scroller");
	var my_mywrapper = document.getElementById(id)
	var scroller = my_mywrapper.children[0];//document.getElementById("scroller");
	var thelist = scroller.children[0];//document.getElementById("thelist");
	var itemslength = thelist.children.length;
	scroller.style.width = 655 +'px';
	
	function scrollAdjustment(e)
	{
		if (id == "menu_wrapper")
		{
			var
			bounds = (705 - window.innerWidth);
			
			if (e.x >= 0)
			{
				document.getElementById('l_menu_arrow').className = 'l_menu_arrow';
			}
			if (e.x < 0)
			{
				document.getElementById('l_menu_arrow').className = 'l_menu_arrow_active';
				document.getElementById('r_menu_arrow').className = 'r_menu_arrow_active';
			}
			if (e.x <= -bounds)
			{
				document.getElementById('r_menu_arrow').className = 'r_menu_arrow';
			}
		}
	}
	
	var myScroll = new iScroll(id,
	{
		hScrollbar  : false,
		onScrollEnd : function() { scrollAdjustment(this); },
		onScrollMove: function() { scrollAdjustment(this); },
	});
	
	gorilla.iscrollscrollers = {}
	gorilla.iscrollscrollers[id] = myScroll;
};

gorilla.preloadFunWallpapers = function()
{
	
	gorilla.FunWallpapers = [ 'assets/funWithGorillaPage/carousel/a0.jpg',
	'assets/funWithGorillaPage/carousel/a1.jpg',
	'assets/funWithGorillaPage/carousel/a2.jpg',
	'assets/funWithGorillaPage/carousel/a3.jpg',
	'assets/funWithGorillaPage/carousel/a4.jpg',
	
	                 ]
	
	for(var i=0;i<gorilla.FunWallpapers.length;i++)
	{
		//alert(gorilla.FunWallpapers[i]);
		var img = new Image;
		img.src = gorilla.FunWallpapers[i];
	}

}
